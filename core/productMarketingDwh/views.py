# Francisco Falise, copyright 01/10/2022


from ftplib import parse257
from pickle import TRUE
from sqlite3 import register_converter
from django.db.models import query
from django.forms.fields import EmailField
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect
from django import template
from .models import *
from productMarketing.models import *
from django_tables2 import SingleTableView
#from .tables import TherapyTable, CommunicationTable
from django.contrib import messages
#from .filters import *
from django.views.generic import ListView, DetailView
from django.http import JsonResponse
import json
import datetime
from django.forms import modelformset_factory
#from .test import *
from django.core.serializers import serialize
from django.forms.models import model_to_dict
#from bootstrap_datepicker_plus.widgets import DatePickerInput, TimePickerInput, DateTimePickerInput, MonthPickerInput, YearPickerInput
from django.forms import formset_factory
# email
from django.core.mail import send_mail
#from core.settings import BASE_DIR, EMAIL_HOST_USER
# email
from django.core import serializers
from django.utils import timezone
from django.template import RequestContext
from django.db import connection
from datetime import date, timedelta
import matplotlib as plt
import matplotlib.pyplot as plt
import io
import base64

####
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
import seaborn as sns
import matplotlib.widgets as mw
import time
#from .diagnosis import decode, decodePrivateInsurers
#from .scan import *
#from .ocr import ocrscan
#from .suitability import physioSuitability

from enum import Enum
from productMarketing.queryJobs.boupDataOverview import *
from productMarketing.queryJobs.warnings import *
from productMarketing.queryJobs.SQL_query import *


@login_required(login_url="/login/")
def showKPI(request):
    projectNameError = True
    print("Start showKPI")
    if request.method == 'POST':

        projectName = request.POST.get('projectName')
        print("Get projects")
        projects = Project.objects.filter(projectName=projectName)
        print("Got projects")
        count = projects.count()
        project = projects[0]
        region1 = project.region
        print(count)
        for i in range(0, count):
            print(i)
            print(projects[i].id)
            print(projects[i].salesName)
            print(projects[i].region)

        years = [2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030, 2031,
                 2032, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045]
        vhk = []
        volumes = []
        volumesCustomer = []
        prices = []
        currencyPrices = ""
        currencyVhk = ""
        region = []
        print("rfp of project", project.salesName.rfp)

        for year in years:
            try:
                #priceObject = ProjectPrices.objects.get(project = project, calenderYear = int(year), valid = True)
                object = ProjectVolumePrices.objects.get(
                    project=project, calendarYear=year)

                #volumeCustObject = ProjectVolumeCustomerEstimation.objects.get(project = project, calenderYear = year)
                #volumeObject = ProjectVolume.objects.get(project = project, calenderYear = year)
                prices.append(float(object.price))
                volumes.append(object.quantity)
                volumesCustomer.append(object.quantityCustomerEstimation)
                currencyPrices = (object.currency)
            except:
                prices.append(0)
                volumes.append(0)
                volumesCustomer.append(0)

        try:
            costObject = VhkCy.objects.get(RFP=project.salesName.rfp.pk)

            for year in years:
                varName = "cy" + str(year)
                try:
                    costValue = getattr(costObject, varName)
                    vhk.append(float(costValue))
                    currencyVhk = costObject.currency.currency
                except:
                    vhk.append(0)
        except:
            # empty vhks
            for year in years:
                vhk.append(0)

        for year in years:
            try:
                region.append(project.region)
            except:
                region.append('Not avaiable')

        ##### revenue and gm
        revenue = []
        grossMargin = []
        grossMarginPct = []

        fxErrorPrices = False

        print("prices", prices)
        print("vhk", vhk)
        print("volumes", volumes)
        print("currencies", currencyPrices, currencyVhk)
        '''
        ### check if price currency matches cost currency, else batch transform
        if currencyPrices != "EUR":
            print("currencies do not match! converting to EUR")
            ### get current rate
            fx = 1.0
            print("get currency")
            currencyObj = Currencies.objects.get(currency = currencyPrices)
            print("got currency")
            try:
                fxObject = ExchangeRates.objects.get(currency = currencyObj.pk, valid = True)
                fx = fxObject.rate
            except:
                print("failed currency exchange!! aborting!!")
                fxErrorPrices = True

            print("lenghts", len(prices), len(years))
            for index in range((len(years) - 1)):
                prices[index] = prices[index] * float(fx)

        if currencyVhk != "EUR":
            print("currencies vhk do not match! converting to EUR")
            ### get current rate
            fx = 1.0
            currencyObj = Currencies.objects.get(currency = currencyVhk)
            print("currobj", currencyObj.currency)

            try:
                fxObject = ExchangeRates.objects.get(currency = currencyObj.pk, valid = True)
                fx = fxObject.rate
            except:
                print("failed currency exchange!! aborting!!")
                fxErrorPrices = True

            for index in range((len(years) - 1)):
                vhk[index] = vhk[index] * float(fx)

        #### will be first in EUR
        print("------------> final results without considering family prices")

'''
        fxErrorPrices == False
        if fxErrorPrices == False:
            for index in range(len(years)):
                revenueValue = prices[index] * volumes[index]
                grossMarginValue = revenueValue - (vhk[index] * volumes[index])
                revenue.append(revenueValue)
                grossMargin.append(grossMarginValue)
                grossMarginPctValue = 0
                try:
                    grossMarginPctValue = grossMarginValue * 100 / revenueValue
                    grossMarginPct.append(grossMarginPctValue)
                except:
                    grossMarginPct.append(0)
                print("%%%%%%%%%%% final result, year", years[index], "volume", volumes[index], "price", prices[index], "currency: EUR",
                      "revenue", revenueValue, "gross margin", grossMarginValue, "cost", vhk[index], "margin pctg", grossMarginPctValue)

        print("lens", len(years), len(grossMargin), len(revenue))
        print("%%%%%&&&&& gross margins", grossMargin)
        print("%%%%%&&&&& revenue", revenue)
        print("%%%%%&&&&& cumulative gross margin", sum(grossMargin))
        print("%%%%%&&&&& cumulative revenue", sum(grossMargin))

        # plots
        plt.switch_backend('agg')

        sns.set_style("darkgrid")

        rev_margin = sns.lineplot(years, revenue, label="Revenue", marker="o")
        sns.lineplot(years, grossMargin, label="Gross Margin", marker="o")
        rev_margin.set(title='Revenue and Gross Margin',
                       xlabel='Year', ylabel='EUR')

        revenueMarginPlot = io.BytesIO()
        plt.savefig(revenueMarginPlot, format='jpg')
        revenueMarginPlot.seek(0)
        revenueMarginPlotBase64 = base64.b64encode(revenueMarginPlot.read())
        plt.clf()

        encodedrevenueMarginPlotBase64 = str(revenueMarginPlotBase64)
        encodedrevenueMarginPlotBase64 = encodedrevenueMarginPlotBase64[2:]
        encodedrevenueMarginPlotBase64 = encodedrevenueMarginPlotBase64[:-1]

        fig, ax = plt.subplots()
        ax2 = ax.twinx()
        volPrice = sns.lineplot(x=years, y=volumes, ax=ax,  marker="o")
        sns.lineplot(years, prices,  ax=ax2, color="orange", marker="o")
        ax.legend(handles=[a.lines[0]
                  for a in [ax, ax2]], labels=["Prices", "Volumes"])
        ax.set_ylabel('Pieces')
        ax2.set_ylabel('EUR')
        volPrice.set(title='Volume and Price')

        volumePricePlot = io.BytesIO()
        plt.savefig(volumePricePlot, format='jpg')
        volumePricePlot.seek(0)
        volumePricePlotBase64 = base64.b64encode(volumePricePlot.read())
        plt.clf()

        encodedvolumePricePlotBase64 = str(volumePricePlotBase64)
        encodedvolumePricePlotBase64 = encodedvolumePricePlotBase64[2:]
        encodedvolumePricePlotBase64 = encodedvolumePricePlotBase64[:-1]

        fig, ax = plt.subplots()
        ax2 = ax.twinx()
        volCPrice = sns.lineplot(
            x=years, y=volumesCustomer, ax=ax,  marker="o")
        sns.lineplot(years, prices,  ax=ax2, color="orange", marker="o")
        ax.legend(handles=[a.lines[0] for a in [ax, ax2]],
                  labels=["Prices", "CustomerVolumes"])
        ax.set_ylabel('Pieces')
        ax2.set_ylabel('EUR')
        volCPrice.set(title='CustomerVolume and Price')

        volumeCPricePlot = io.BytesIO()
        plt.savefig(volumeCPricePlot, format='jpg')
        volumeCPricePlot.seek(0)
        volumeCPricePlotBase64 = base64.b64encode(volumeCPricePlot.read())
        plt.clf()

        plt.close()

        encodedvolumeCPricePlotBase64 = str(volumeCPricePlotBase64)
        encodedvolumeCPricePlotBase64 = encodedvolumeCPricePlotBase64[2:]
        encodedvolumeCPricePlotBase64 = encodedvolumeCPricePlotBase64[:-1]

    else:
        projectName = ""
        count = 0
        region1 = ""
        projectNameError = ""
        encodedrevenueMarginPlotBase64 = ""
        encodedvolumePricePlotBase64 = ""
        encodedvolumeCPricePlotBase64 = ""
        print("no project name")

    return render(request, "productMarketing/KPI.html", {'projectNameError': projectNameError, 'count': count, 'region': region1, 'projectName': projectName, 'revenueMarginPlotBase64': encodedrevenueMarginPlotBase64, 'volumePricePlotBase64': encodedvolumePricePlotBase64, 'SliderPlotBase64': encodedvolumeCPricePlotBase64})


# for the Overviews... the first views return the HTML template. the following are the AJAX endpoints. The slim view shows only the key data of a project.

def boupTableSlim(request):
    print("########### running boupTable slim")

    if request.method == 'POST':
        print("post")

    return render(request, "productMarketing/boupOverviewSlim.html", {'segment': 'boupOverviewSlim', })


def boupTable(request):
    print("########### running boupTable")

    if request.method == 'POST':
        print("post")

    return render(request, "productMarketing/boupOverview.html", {'segment': 'boupOverviewSlim', })


def boupTable_FY(request):
    print("########### running boup table FY")

    if request.method == 'POST':
        print("post")

    return render(request, "productMarketing/boupOverview_FY.html", {'segment': 'boupOverviewSlim', })


def boupTableSlimData(request):
    print("running boupTableSlimData")

    outputArray, totalVolumes, totalVolumesWeighted, totalRevenues, totalRevenuesWeighted, totalMargin, totalMarginWeighted, totalAsps, totalAspsWeighted, years = getBoupTableData(
        request, False)
    outputDict = dict()
    outputDict['dataForTable'] = outputArray
    outputDict['totalVolumes'] = totalVolumes
    outputDict['totalVolumesWeighted'] = totalVolumesWeighted
    outputDict['totalRevenues'] = totalRevenues
    outputDict['totalRevenuesWeighted'] = totalRevenuesWeighted
    outputDict['totalMargin'] = totalMargin
    outputDict['totalMarginWeighted'] = totalMarginWeighted
    outputDict['totalAsps'] = totalAsps
    outputDict['totalAspsWeighted'] = totalAspsWeighted
    outputDict['years'] = years
    return JsonResponse(outputDict, safe=False)


def boupTableAllData(request):
    print("running boupTableAllData")

    outputArray, totalVolumes, totalVolumesWeighted, totalRevenues, totalRevenuesWeighted, totalMargin, totalMarginWeighted, totalAsps, totalAspsWeighted, years = getBoupData(
        request, True)
    outputDict = dict()
    outputDict['dataForTable'] = outputArray
    outputDict['totalVolumes'] = totalVolumes
    outputDict['totalVolumesWeighted'] = totalVolumesWeighted
    outputDict['totalRevenues'] = totalRevenues
    outputDict['totalRevenuesWeighted'] = totalRevenuesWeighted
    outputDict['totalMargin'] = totalMargin
    outputDict['totalMarginWeighted'] = totalMarginWeighted
    outputDict['totalAsps'] = totalAsps
    outputDict['totalAspsWeighted'] = totalAspsWeighted
    outputDict['years'] = years
    print("Dict gernerated")
    return JsonResponse(outputDict, safe=False)
