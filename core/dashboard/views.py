from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.


class Dashboard(LoginRequiredMixin, View):
    template_name = "dashboard/dashboard.html"

    def get(self, request, *args, **kwargs):
        # add data here to render to the template
        context = {"heading": "Dashboard", "pageview": "Dashboard"}
        return render(request, self.template_name, context)
