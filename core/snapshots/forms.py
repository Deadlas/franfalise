from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator

# from betterforms.multiform import MultiForm, MultiModelForm

from .models import SnapshotMetaData


class CreateSnapshotForm(forms.ModelForm):

    class Meta:
        model = SnapshotMetaData
        fields = (
            # first tab fields
            "user",
            "snapshotName",
            "tag",
            "fileName"
        )
