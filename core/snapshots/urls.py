from django.urls import path
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('productMarketing/snapshots', views.getSnapshotList.as_view()),
    path('productMarketing/snapshotList',
         views.viewSnapshotList, name='snapshotList'),
    path('productMarketing/snapshots/<int:snapshotId>',
         views.snapshot.as_view(), name='snapshot'),
    path('productMarketing/view_snapshot/<int:snapshotId>',
         views.snapshotDeepdive, name='snapshotDeepdive'),
    path('productMarketing/deleteSnapshot/<int:snapshotId>',
         views.snapshotDelete, name='snapshotDelete'),

    path('productMarketing/createSnapshotTag',
         views.createSnapshotTag, name='createSnapshotTag'),


    # for filters
    path('productMarketing/snapshotTagList',
         views.snapshotTagList, name='snapshotTagList'),
    path('productMarketing/snapshotUserList',
         views.snapshotUserList, name='snapshotUserList'),
    path('productMarketing/newSnapshot', views.nweSnapshot, name='newSnapshot'),
    path('productMarketing/snapshotCompare',
         views.snapshotCompare, name='snapshotCompare'),
    path('productMarketing/snapshot', views.snapshot.as_view()),

]
