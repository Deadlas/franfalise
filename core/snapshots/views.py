from .models import *
from django.http import JsonResponse, HttpResponseRedirect
from datetime import date, timedelta
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render, get_object_or_404, redirect
from .serializers import *
from django.contrib.auth.decorators import login_required
import pandas as pd
import csv
from .forms import CreateSnapshotForm
from django.http import HttpResponse
from django.contrib.auth import get_user_model
from django.core import serializers
from django.urls import reverse_lazy

"""
* get all snapshots list -> queryset

fields:
date,
user,
snapshotName,
tag

"""


class getSnapshotList(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication]

    # jsut a dummy
    # SnapshotMetaData.objects.get_or_create(snapshotName="testsnapshot")

    ###
    serializer_class = SnapshotSerializer
    queryset = SnapshotMetaData.objects.all()


"""
* download CSV of selected snapshot
* delete a snapshot
"""


class snapshot(APIView):
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication]

    def post(self, request):
        # snapshotForm = CreateSnapshotForm(request.POST)
        # if snapshotForm.is_valid():

        fileName = "./persistentLayer/test.csv"
        snapshotNameInput = request.POST.get('snapshotNameInput')
        snapshotCommentsInput = request.POST.get('snapshotCommentsInput')
        tagInput = request.POST.get('tagInput')
        user = request.user

        tagObj = SnapshotTags.objects.get(id=tagInput)

        snapshotObject, created = SnapshotMetaData.objects.get_or_create(
            user=user, snapshotName=snapshotNameInput, tag=tagObj, fileName=fileName, snapshotComments=snapshotCommentsInput)

        url = '/ifx/productMarketing/view_snapshot/' + str(snapshotObject.id)
        return HttpResponseRedirect(url)

    # for downloading the requested snapshot as a CSV

    def get(self, request, snapshotId):
        print("################ status", snapshotId)

        try:
            # here logic to retrieve a snapshot from persistent storage
            fileName = SnapshotMetaData.objects.get(id=snapshotId)
            fileName = "test"
            print(fileName)
            fullFilePath = "./persistentLayer/snapshotStorage/" + fileName + ".csv"
            # to do: how to avoid loading into pandas??
            df1 = pd.read_csv(fullFilePath, sep=';', decimal=",")

            response = HttpResponse(
                content_type='text/csv',
                headers={
                    'Content-Disposition': 'attachment; filename="testsnapshot.csv"'},
            )

            # with other applicable parameters
            df1.to_csv(path_or_buf=response)
            return response

        #     df_boup = pd.DataFrame(list(BoUp.objects.filter(id = boup_id).values()))

        except:
            print("failed to fetch snapshot CSV")
            return HttpResponse(status=401)

    # for deleting the requested snapshot

    def delete(self, request, format=None):
        snapshotId = request.query_params.get('snapshotId', None)

        # here logic to delete a snapshot
        # SnapshotMetaData.objects.filter(id=snapshotId).delete()

        return HttpResponse(status=200)


"""
* open one snapshot  (open a new view showing the deepdive of the selected snapshot using an URL)
"""

# screen for snapshot list, tbd if with data tables.


@login_required(login_url="/login/")
def viewSnapshotList(request):
    print("###### view snapshots")
    data = "asd123"
    return render(request, "snapshots/list.html", {'data': data, })

# screen for deepdiving a selected snapshot, tbd if with data tables.


@login_required(login_url="/login/")
def snapshotDelete(request, snapshotId):
    print("###### snapshotDelete")

    SnapshotMetaData.objects.filter(id=snapshotId).delete()
    return HttpResponse(status=200)


@login_required(login_url="/login/")
def snapshotDeepdive(request, snapshotId):
    print("###### snapshotDeepdive")

    data = SnapshotMetaData.objects.get(id=snapshotId)
    return render(request, "snapshots/deepdive.html", {'data': data})


@login_required(login_url="/login/")
def snapshotTagList(request):
    allSnapshotTags = SnapshotTags.objects.all()
    # data = serializers.serialize("json", allSnapshotTags) -> strange nested format
    outputArray = []

    for tag in allSnapshotTags:
        tempDict = dict()
        tempDict['tagName'] = tag.tagName
        tempDict['user_id'] = tag.user_id
        tempDict['id'] = tag.pk
        outputArray.append(tempDict)

    return JsonResponse(outputArray, safe=False)


@login_required(login_url="/login/")
def createSnapshotTag(request):
    if request.method == 'POST':
        tagNameInput = request.POST.get('tagName')
        user = request.user
        SnapshotTags.objects.get_or_create(user=user, tagName=tagNameInput)
        return HttpResponse(status=200)
    else:
        return HttpResponse(status=500)


@login_required(login_url="/login/")
def nweSnapshot(request):
    return render(request, "snapshots/newSnapshot.html")

# view mit drop downs


@login_required(login_url="/login/")
def snapshotCompare(request):
    if request.method == 'POST':
        baseline1 = request.POST.get('baseline1')
        baseline2 = request.POST.get('baseline2')
        #request.session["baseline1"] = baseline1
        #request.session["baseline2"] = baseline2

        years = []
        revenueValuesBaseline1 = []
        revenueValuesBaseline2 = []
        profitValuesBaseline1 = []
        profitValuesBaseline2 = []

        if not baseline1:
            print("baseline1 missing from session! redirecting")
            return render(request, "snapshots/snapshotCompare.html")

        if not baseline2:
            print("baseline2 missing from session! redirecting")
            return render(request, "snapshots/snapshotCompare.html")

        context = {"years": years, "revenueValuesBaseline1": revenueValuesBaseline1, "revenueValuesBaseline2": revenueValuesBaseline2,
                   "profitValuesBaseline1": profitValuesBaseline1, "profitValuesBaseline2": profitValuesBaseline2, "isInspect": True}
        #success_url = reverse_lazy("snapshotCompareDeepDive")
        # return HttpResponseRedirect(success_url)
        return render(request, "snapshots/snapshotCompare.html", context)

    return render(request, "snapshots/snapshotCompare.html")


@login_required(login_url="/login/")
def snapshotCompareDeepDive(request):

    years = []
    revenueValuesBaseline1 = []
    revenueValuesBaseline2 = []
    profitValuesBaseline1 = []
    profitValuesBaseline2 = []

    # here to logic to populate arrays with relevant data
    baseline1 = request.session["baseline1"]
    baseline2 = request.session["baseline2"]
    redirect_url = reverse_lazy("snapshotCompare")

    if not baseline1:
        print("baseline1 missing from session! redirecting")
        return HttpResponseRedirect(redirect_url)

    if not baseline2:
        print("baseline2 missing from session! redirecting")
        return HttpResponseRedirect(redirect_url)

    context = {"years": years, "revenueValuesBaseline1": revenueValuesBaseline1, "revenueValuesBaseline2": revenueValuesBaseline2,
               "profitValuesBaseline1": profitValuesBaseline1, "profitValuesBaseline2": profitValuesBaseline2}

    return render(request, "snapshots/snapshotCompareDeepDive.html", context)


@login_required(login_url="/login/")
def snapshotUserList(request):
    User = get_user_model()
    users = User.objects.all()
    print("user list", users)

    outputArray = []

    for user in users:
        tempDict = dict()
        tempDict['username'] = user.username
        tempDict['firstName'] = user.first_name
        tempDict['lastName'] = user.last_name
        tempDict['id'] = user.pk
        outputArray.append(tempDict)

    return JsonResponse(outputArray, safe=False)
