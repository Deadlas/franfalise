# Francisco Falise, copyright 01/10/2022
# new way using adolphs List

import pandas as pd
from ..models import *
from datetime import datetime
from django.utils import timezone
import pandasql as ps
from enum import Enum
from typing import (
    Iterable,
    TypeVar,
    MutableSequence,
    MutableSequence,
    MutableSequence,
    Any,
)


def getSeriesPl58(df):
    seriesList = []
    dfpl58 = df.loc[((df["PL"] == "58")), :]

    # if RFP is in TCx... get the series
    queryUniqueRfp = """SELECT DISTINCT RFPProduct FROM df ORDER BY RFPProduct ASC;"""

    queryResultUniqueRfp = ps.sqldf(queryUniqueRfp, locals())[
        "RFPProduct"].tolist()

    for rfp in queryResultUniqueRfp:
        if ("TC1" in rfp) | ("TC2" in rfp) | ("TC3" in rfp) | ("TC4" in rfp):
            if len(rfp) > 3:
                tcName = rfp[4:7]
                stringName = rfp[7]
                thisdict = {
                    "tcName": tcName,
                    "series": stringName,
                }

                seriesList.append(thisdict)
                print(rfp, "adding series", stringName)

    seriesList = [i for n, i in enumerate(
        seriesList) if i not in seriesList[n + 1:]]
    print("----> final series list")
    print(seriesList)

    return seriesList


def adolfRun():
    now = datetime.now()
    date = now.strftime("%Y%m%d")
    totalImportErrors = []

    # Block A: loading, cleaning job
    df1 = pd.read_csv(
        "./persistentLayer/importJobs/adolphList.csv",
        sep=";",
        decimal=",",
        low_memory=False,
        skiprows=7,
    )

    # df1 = pd.read_csv("./adolphList.csv", sep = ';', decimal=",", low_memory=False, skiprows=7)
    df1 = df1.drop([0])
    df1 = df1.fillna("None")

    # df1 = df1.reset_index(drop=True)

    # df1 = pd.read_csv("./productMarketing/importJobs/adolphList.csv", sep = ';', decimal=",")
    # df1 = df1.drop([0, 1, 2, 3, 4, 5, 6])
    # df1 = df1.reset_index(drop=True)

    # rename PPOS match (its an empty column)
    cols = pd.Series(df1.columns)
    dup_count = cols.value_counts()
    for dup in cols[cols.duplicated()].unique():
        cols[cols[cols == dup].index.values.tolist()] = [
            dup + str(i) for i in range(1, dup_count[dup] + 1)
        ]

    df1.columns = cols
    # df1.to_csv("./test3.csv", sep = ";", decimal=",", header=True)
    # for now everything handled as strings, PPOS is duplicated! we need PPOS.1
    df1 = df1[
        [
            "DIV",
            "BL",
            "PL",
            "PL Desc",
            "PL_HFG",
            "HFG",
            "HFG Description",
            "Marketing Contact",
            "Basic Type (max)",
            "RFP Product",
            "Product Maturity",
            "Product Family *",
            "Main Family *",
            "Sales Product Name",
            "SP#",
            "Package (PPOS)",
            "Package (CRM)",
            "PPOS.1",
            "Quality Requirement Category",
        ]
    ]

    # rename PPOS.1 to PPOS
    df1 = df1.rename(columns={"PPOS.1": "PPOS"})

    # keep only pl hfg 58_x or 90_x
    df1 = df1.loc[((df1["PL_HFG"].str.contains("58_")) |
                   (df1["PL_HFG"].str.contains("90_"))), :]

    # remove HFG 9 (pl58) and S (both pls)
    df1 = df1.loc[
        (((df1["PL"] == "58") & (df1["HFG"] != "9")) | (df1["PL"] == "90")), :
    ]
    # df1 = df1.loc[(((df1['PL'] =='58') & (df1['HFG'] != '9')) ), :]

    df1 = df1[df1["HFG"] != "S"]

    df1.sort_values("PL")

    # remove dummy products
    df1 = df1[df1["Quality Requirement Category"] != "NOT APPLICABLE"]

    df1 = df1[df1["Sales Product Name"].str.contains("DAISY") == False]

    df1 = df1.rename(columns={"Product Family *": "ProductFamily"})
    df1 = df1.rename(columns={"HFG Description *": "HFGDescription"})
    df1 = df1.rename(columns={"Basic Type (max)": "BasicTypeMax"})
    df1 = df1.rename(columns={"RFP Product": "RFPProduct"})
    df1 = df1.rename(columns={"Product Maturity": "ProductMaturity"})
    df1 = df1.rename(columns={"Sales Product Name": "SalesName"})
    df1 = df1.rename(columns={"SP#": "SPNumber"})
    df1 = df1.rename(columns={"Package (PPOS)": "PackagePPOS"})
    df1 = df1.rename(columns={"Package (CRM)": "PackageCRM"})
    df1 = df1.rename(
        columns={"Quality Requirement Category": "QualityRequirementCategory"}
    )
    df1 = df1.rename(columns={"Main Family *": "MainFamily"})

    df1 = df1[df1["ProductFamily"] != None]
    df1 = df1[df1["ProductFamily"] != "None"]
    df1 = df1[df1["ProductFamily"] != ""]
    df1 = df1.reset_index(drop=True)
    length = len(df1.index)

    df1.to_csv("./filteredProductList1.csv", sep=";", decimal=",", header=True)

    for index in range(0, length, 1):
        uncasted = df1.loc[index, "ProductFamily"]
        rfpProduct = df1.loc[index, "RFPProduct"]

        value = str(df1.loc[index, "ProductFamily"])

        if len(value) == 0:
            df1.loc[index, "ProductFamily"] = "delete"
            print(
                "evaluating",
                value,
                "type",
                type(value),
                "len",
                len(value),
                "isspace",
                value.isspace(),
            )

        if value.isspace() == True:
            df1.loc[index, "ProductFamily"] = "delete"
            print(
                "evaluating",
                value,
                "type",
                type(value),
                "len",
                len(value),
                "isspace",
                value.isspace(),
            )

        if value == "None":
            df1.loc[index, "ProductFamily"] = "delete"
            print(
                "evaluating",
                value,
                "type",
                type(value),
                "len",
                len(value),
                "isspace",
                value.isspace(),
            )

        if value == "":
            df1.loc[index, "ProductFamily"] = "delete"
            print(
                "evaluating",
                value,
                "type",
                type(value),
                "len",
                len(value),
                "isspace",
                value.isspace(),
            )

    # df1 = df1[df1.apply(lambda x : "delete" if (len(str(x['ProductFamily'])) == 0) else x,axis=1)]

    df1 = df1[df1["ProductFamily"] != "delete"]
    df1 = df1.reset_index(drop=True)
    # df1 = df1[len(df1['ProductFamily']).values > 0]
    # df1 = df1[(df1['ProductFamily']).values.isspace() == False]

    # df1 = df1[df1['Sales Product Name'] not in "DAISY"]
    df1 = df1.reset_index(drop=True)
    print("df 1")
    print(df1)

    # generate family names
    familyNames: MutableSequence[str] = []

    queryFamilyNames = (
        """SELECT DISTINCT MainFamily FROM df1 ORDER BY MainFamily ASC;"""
    )
    queryResultFamilyNames = ps.sqldf(queryFamilyNames, locals())[
        "MainFamily"].tolist()
    (print("##### queryResultFamilyNames", queryResultFamilyNames))

    ProductFamily.objects.update(valid=False)

    # create entries of product family tables
    for family in queryResultFamilyNames:
        # print("creating family", family)
        obj, created = ProductFamily.objects.get_or_create(
            family_name=str(family))
        obj.importDate = datetime.now(tz=timezone.utc)
        obj.valid = True
        obj.save()

        # now add the related series to it

    # now we need to generate the series, if possible
    length = len(df1.index)

    for index in range(0, length, 1):
        seriesValue = ""
        familyName = df1.loc[index, "ProductFamily"]
        if "AURIX_TC" in str(familyName):
            seriesValue = familyName[-1]
            df1.loc[index, "Series"] = seriesValue
            df1.loc[index, "PackageShortName"] = df1.loc[index, "RFPProduct"][8]
        else:
            seriesValue = str(familyName)
            df1.loc[index, "Series"] = seriesValue
            df1.loc[index, "PackageShortName"] = ""

    queryPackages = """SELECT DISTINCT MainFamily, Series, PackagePPOS, PackageShortName FROM df1 ORDER BY PackagePPOS ASC;"""
    queryResultPackages = ps.sqldf(
        queryPackages, locals())  # ['PackagePPOS'].tolist()

    # remove duplicates
    queryResultFamilyNames = list(dict.fromkeys(queryResultFamilyNames))
    # queryResultPackages = list(dict.fromkeys(queryResultPackages))

    print("##### queryResultPackages")
    print(queryResultPackages)
    # generate product series for PL58, microcontrollers
    # seriesList = getSeriesPl58(df1)

    querySeries = (
        """SELECT DISTINCT MainFamily, Series FROM df1 ORDER BY MainFamily ASC;"""
    )
    # ['PackagePPOS'].tolist()
    querySeriesResult = ps.sqldf(querySeries, locals())

    ProductSeries.objects.update(valid=False)
    print("query series result %%%%%%%%%%")
    print(querySeriesResult)
    print("%%%%%%%%%%%%%%%%%")

    for index in range(0, len(querySeriesResult.index), 1):
        familyName = querySeriesResult.loc[index, "MainFamily"]
        familyObject = ProductFamily.objects.get(family_name=familyName)
        series = querySeriesResult.loc[index, "Series"]
        seriesObj, created = ProductSeries.objects.get_or_create(
            family=familyObject, series=series
        )
        seriesObj.importDate = datetime.now(tz=timezone.utc)
        seriesObj.valid = True
        seriesObj.save()
        print(index, "creating series", familyName,
              "--", series, "obj", seriesObj)

    """
    for familyObject in familyObjects:

        if "AURIX_TC" in familyObject.family_name:
            seriesValue = familyObject.family_name[-1]
            #print("creating new series", seriesValue)

        else:
            seriesValue = familyObject.family_name
           # print("creating new series", seriesValue)
            seriesObj, created = ProductSeries.objects.get_or_create(family = familyObject, series=seriesValue)
            seriesObj.importDate = datetime.now(tz=timezone.utc)
            seriesObj.valid = True
            seriesObj.save()
    """

    # create packages
    ProductPackage.objects.update(valid=False)
    productSeriesObjects = ProductSeries.objects.all()
    queryResultPackages.to_csv(
        "./queryResultPackages.csv", sep=";", decimal=",", header=True
    )

    for index in range(0, len(queryResultPackages.index), 1):
        series = queryResultPackages.loc[index, "Series"]
        packagePpos = queryResultPackages.loc[index, "PackagePPOS"]
        family = queryResultPackages.loc[index, "MainFamily"]
        # print("product family", family, "type", type(family), "len", len(str(family)), "package", packagePpos)
        packageDescription = queryResultPackages.loc[index, "PackageShortName"]
        familyObj = ProductFamily.objects.get(family_name=family)
        seriesObj = ProductSeries.objects.get(series=series, family=familyObj)
        packageObj, created = ProductPackage.objects.get_or_create(
            package=packagePpos, series=seriesObj
        )
        packageObj.valid = True
        packageObj.importDate = datetime.now(tz=timezone.utc)

        # if aurix line, add the shortname for package
        packageObj.description = packageDescription
        packageObj.save()

    # create products
    queryProducts = """SELECT DISTINCT RFPProduct, Series, MainFamily, BasicTypeMax, PackagePPOS, HFG, PPOS, SalesName FROM df1 ORDER BY MainFamily ASC;"""
    queryResultProducts = ps.sqldf(
        queryProducts, locals()
    )  # ['ProductFamily'].tolist()

    (print("##### queryResultProducts"))
    (print(queryResultProducts))

    df1.to_csv("./filteredProductList2.csv", sep=";", decimal=",", header=True)
    queryResultProducts.to_csv(
        "./queryResultProducts.csv", sep=";", decimal=",", header=True
    )

    processedRfps = []
    rfpObjects = []

    df1 = df1.reset_index(drop=True)

    querSalesNameCount = """SELECT COUNT(DISTINCT SalesName) FROM df1;"""
    queryResultSalesNameCount = ps.sqldf(
        querSalesNameCount, locals()
    )

    ####
    df1.to_csv("./df1TestSalesNamesUnique.csv",
               sep=";", decimal=",", header=True)

    querySalesNameDuplicates = """SELECT * FROM df1 WHERE SalesName in (SELECT SalesName FROM ( SELECT SalesName, RFPProduct FROM df1 GROUP BY  SalesName, RFPProduct) GROUP BY SalesName HAVING COUNT(*) > 1);"""
    queryResultSalesNameDuplicates = ps.sqldf(
        querySalesNameDuplicates, locals()
    )

    queryResultSalesNameDuplicates.to_csv("./df1TestSalesNamesUniqueReport.csv",
                                          sep=";", decimal=",", header=True)

    #print("query salesname count", queryResultSalesNameCount)

    for index in range(0, len(df1.index), 1):
        rfp = df1.loc[index, "RFPProduct"]
        if rfp not in processedRfps:
            series = df1.loc[index, "Series"]
            packagePpos = df1.loc[index, "PackagePPOS"]
            family = df1.loc[index, "MainFamily"]
            familyObj = ProductFamily.objects.get(family_name=family)
            seriesObj = ProductSeries.objects.get(
                series=series, family=familyObj)
            packageObj = ProductPackage.objects.get(
                package=packagePpos, series=seriesObj
            )
            processedRfps.append(rfp)

            basicTypeMax = df1.loc[index, "BasicTypeMax"]

            productObject, created = Product.objects.get_or_create(rfp=rfp)
            productObject.hfg = df1.loc[index, "HFG"]
            productObject.ppos = df1.loc[index, "PPOS"]
            productObject.basicType = basicTypeMax
            productObject.availablePGS = True

            productObject.package = packageObj
            productObject.save()
            rfpObjects.append(productObject)

            salesName = df1.loc[index, "SalesName"]
            if (len(basicTypeMax) > 0) and (basicTypeMax.isspace() == False):
                salesNameObj, created = SalesName.objects.get_or_create(
                    name=salesName)
                salesNameObj.rfp = productObject
                salesNameObj.save()

            """
            print(
                "finished created product and salesname",
                index,
                "info",
                productObject,
                "rfp",
                productObject.rfp, "salesname", salesNameObj
            )
            """

        else:
            indexA = processedRfps.index(rfp)
            productObject = rfpObjects[indexA]
            salesName = df1.loc[index, "SalesName"]
            if (len(basicTypeMax) > 0) and (basicTypeMax.isspace() == False):
                salesNameObj, created = SalesName.objects.get_or_create(
                    name=salesName)
                salesNameObj.rfp = productObject
                salesNameObj.save()

            #print("quick created product and salesname", index, "info", productObject, salesNameObj)

        if len(processedRfps) > 100:
            processedRfps = processedRfps[:100]
            rfpObjects = rfpObjects[:100]

        ####
        """
        querySalesNames = 'SELECT DISTINCT RFPProduct, SalesName FROM df1 WHERE RFPProduct = '+ '"' + rfp + '"' + ' ORDER BY RFPProduct ASC;'
        print("executing query:", querySalesNames )
        queryResultSalesNames= ps.sqldf(querySalesNames, locals()) #['ProductFamily'].tolist()

        for index in range(0, len(queryResultSalesNames.index) , 1):
        """

    print("################# FINISHED IMPORTING SALES NAMES AND PRODUCTS")
    processedRfps = []
    rfpObjects = []

    """
    hfg = models.CharField(max_length=20, blank=True, null=True)
    ppos = models.CharField(max_length=30, blank=True, null=True)
    rfp = models.CharField(max_length=40, blank=True, null=True)
    package = models.ForeignKey(ProductPackage, on_delete=models.SET_NULL, null=True)
    basicType = models.CharField(max_length=40, blank=True, null=True)
    availablePGS = models.CharField(max_length=40, blank=True, null=True)
    valid = models.BooleanField(default=False, blank=True, null=True)
    importDate = models.DateTimeField(default=now, editable=True, blank=True, null=True)
    """

    # we need the package matching the

    # create sales names


# adolfRun()
