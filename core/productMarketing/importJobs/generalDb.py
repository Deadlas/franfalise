# tier 1, oems
# missing

import pandas as pd
from .mainCustomer import *
from .vhkl import *
from .salesNames import *
from ..models import *
from .distributorsImport import *
from .emsImport import *
from .tierOneImport import *
from .productImport import adolfRun

# oems


def runOemImport():
    df1 = pd.read_csv(
        "./persistentLayer/importJobs/oemList.csv", sep=";", decimal=",")
    length = len(df1.index)
    index = 0
    print("### input")
    print(df1)
    print("####")
    for i in range(0, length, 1):

        oemInput = df1.loc[i, "oemName"]
        oem = OEM.objects.filter(oemName=oemInput)

        print("inputs", oemInput, "products", oem)
        if oem.count() > 0:
            print("found more than 0 rps!")

            # what with sales name daisy chain wafer?

            if oem.count() == 1:

                oemObj, created = OEM.objects.get_or_create(oemName=oemInput)
                if created == True:
                    print(index, "created", oemObj)
                else:
                    print(index, "retrieved", oemObj)

            else:
                # tbd
                print("found more than one matching customer!!!")
                # either warning, automated email and then manual import or call support

        else:
            print("oem did not exist! creating")
            oemObj, created = OEM.objects.get_or_create(oemName=oemInput)
            if created == True:
                print(index, "created", oemObj)
            else:
                print(index, "retrieved", oemObj)

    return True


# application main and app detail
def runAppMainImport():

    ApplicationMain.objects.all().delete()
    df1 = pd.read_csv(
        "./persistentLayer/importJobs/applicationMain.csv", sep=";", decimal=","
    )
    length = len(df1.index)
    index = 0
    print("### AppMain input")
    print(df1)
    print("####")

    for i in range(0, length, 1):
        appMainInput = df1.loc[i, "appMainDescription"]
        appMain = ApplicationMain.objects.filter(
            appMainDescription=appMainInput)

        print("---> App Main inputs", appMainInput, "products", appMain)
        if appMain.count() > 0:
            print("found more than 0 appMains!")

            # what with sales name daisy chain wafer?

            if appMain.count() == 1:

                appMainObj, created = ApplicationMain.objects.get_or_create(
                    appMainDescription=appMainInput
                )
                if created == True:
                    print(index, "created", appMainObj)
                else:
                    print(index, "retrieved, doing nothing", appMainObj)

            else:
                # tbd
                print("found more than one matching app main!!!")
                # either warning, automated email and then manual import or call support

        else:
            print("app main did not exist! creating", appMainInput)
            appMainObj, created = ApplicationMain.objects.get_or_create(
                appMainDescription=appMainInput
            )
            print("got ->", appMainObj)
            if created == True:
                print(index, "created", appMainObj)
            else:
                print(index, "retrieved", appMainObj)

    return True


# application main and app detail
def runAppDetailImport():
    df1 = pd.read_csv(
        "./persistentLayer/importJobs/applicationDetail.csv", sep=";", decimal=","
    )
    length = len(df1.index)
    index = 0
    print("!!!!!!!!!!!!!!!!!!!!!!!11")
    print("all OEMs!", OEM.objects.all())
    print("all app mains", ApplicationMain.objects.all())
    print("!!!!!!!!!!!!!!!!!!!!!!!11")
    appMains = ApplicationMain.objects.all()

    firstPk = appMains[0].pk
    for appMainObj in appMains:
        print("----> app main", appMainObj.pk)

    print("### AppDetail input")
    print(df1)
    print("####")

    for i in range(0, length, 1):
        appDetailInput = df1.loc[i, "appDetailDescription"]
        appDetail = ApplicationDetail.objects.filter(
            appDetailDescription=appDetailInput
        )
        # watchout, DB beings counting at 1!!!
        appDetailFk = int(df1.loc[i, "appMain"]) + 1

        print(
            "-----> inputs AppDetail",
            appDetailInput,
            "app detail",
            appDetail,
            "app detail fk (app main):",
            appDetailFk,
        )
        if appDetail.count() > 0:
            print("found more than 0 app details!")

            if appDetail.count() == 1:
                appMainObj = ApplicationMain.objects.get(pk=appDetailFk)

                appDetailObj, created = ApplicationDetail.objects.get_or_create(
                    appDetailDescription=appDetailInput, appMain=appMainObj
                )
                if created == True:
                    print(index, "created", appDetailObj)
                else:
                    print(index, "retrieved", appDetailObj)

            else:
                # tbd
                print("found more than one matching app detail!!!")
                # either warning, automated email and then manual import or call support

        else:
            appMainId = df1.loc[i, "appMain"] + firstPk
            print(
                "######### appDetail did not exist! creating, testing",
                appDetailFk,
                "app main id",
                appMainId,
            )

            appMainObj = ApplicationMain.objects.get(id=appMainId)
            appDetailObj, created = ApplicationDetail.objects.get_or_create(
                appDetailDescription=appDetailInput, appMain=appMainObj
            )
            if created == True:
                print(index, "created", appDetailObj)
            else:
                print(index, "retrieved", appDetailObj)

    return True


# oems
def runCurrenciesImport():
    df1 = pd.read_csv(
        "./persistentLayer/importJobs/currencies.csv", sep=";", decimal=","
    )
    length = len(df1.index)
    index = 0
    print("### input currencies")
    print(df1)
    print("####")
    for i in range(0, length, 1):

        currencyInput = df1.loc[i, "currency"]
        currency = Currencies.objects.filter(currency=currencyInput)

        if currency.count() > 0:
            print("found more than 0 rps!")

            # what with sales name daisy chain wafer?

            if currency.count() == 1:

                currencyObj, created = Currencies.objects.get_or_create(
                    currency=currencyInput
                )
                if created == True:
                    print(index, "created", currencyObj)
                else:
                    print(index, "retrieved", currencyObj)

            else:
                # tbd
                print("found more than one matching customer!!!")
                # either warning, automated email and then manual import or call support

        else:
            print("currency did not exist! creating currency")
            currencyObj, created = Currencies.objects.get_or_create(
                currency=currencyInput
            )
            if created == True:
                print(index, "created", currencyObj)
            else:
                print(index, "retrieved", currencyObj)

    return True


# insert test marketer


def runTestMarketer():
    legalEntity = LegalEntities.objects.get_or_create(
        leShortName="IFAG", leLongName="Infineon Technologies AG"
    )
    print("legal entity", legalEntity)
    marketerObj, created = marketerMetadata.objects.get_or_create(
        name="Test",
        familyName="Marketer",
        country="Germany",
        legalEntity=legalEntity[0],
    )
    # userRole = models.CharField(max_length=15, choices=ALLOWABLE_TYPES_USER_ROLE, blank=True, null=True)


#    businessLine = models.CharField(max_length=15, choices=ALLOWABLE_TYPES_BL, blank=True, null=True, default = "ATV.PSE")


# check general import
def runConfigImport():
    df1 = pd.read_csv(
        "./persistentLayer/importJobs/config.csv", sep=";", decimal=",")
    length = len(df1.index)
    index = 0
    print("###### config file")
    print(df1)
    print("####")
    for i in range(0, length, 1):
        configInput = df1.loc[i, "configName"]
        valueInput = df1.loc[i, "value"]
        print("inputs", configInput)

        if configInput == "firstImport":
            # so if this is the first import, run the full import
            if str(valueInput) == "0":

                runMainCustomerImport()
                runFinalCustomerImport()
                runCurrenciesImport()
                # adolfRun()
                runAppMainImport()
                runAppDetailImport()
                runOemImport()
                runTestMarketer()
                runDistributorsImport()
                runEmsImport()
                runTierOneImport()

                """
                adolfRun()
                return
                """
                """
                print("running full import!")

                runvhklImport()
                """
                print("adolf run finished")
                return

                # workaround for chained dropdowns prefilling of end customer
                # runFinalCustomerImport()
                # runFinalCustomerImportWorkaround()

            else:
                print("skipping data import")
