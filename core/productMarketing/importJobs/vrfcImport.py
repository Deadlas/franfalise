"""
Infineon Technologies AG
With this file we import data from the csv based VRFC extracts. In order to generate this input file, the Cube_VRFC.xlsb file has to be saved as a plain CSV with UTF-8 encoding.
Before saving the CSV, the rows 1 to 9 (inclusive) have to be deleted in the .xlsb file. So the first row is the one with "PM_FC_VIST_QTY...." etc in column F.
The header has to have this format:

Having identity keys in the first column will lead to a rejected import job.

Then, the sales forecast has to be separated form marketing forecast and orders on hand.
After import of this file, first the combination of main and end customers is stored as binding in our database for new entries.

Then, cross checks with the fields suggestedMainCustomerVrfc and suggestedEndCustomerVrfc of t_project are done. Errors are dumped into the table vrfcCrossCheck

If existing entries of t_project do not match the allowable combinations of main+end customer a given batch date, warnings are generated. By no means objects of t_project should be deleted.
Again, keep in mind that the checks are done at the suggestedMainCustomerVrfc level due to the fuzziness of the supply chain.
"""


from cmath import nan
import pandas as pd
import numpy as np
from typing import (
    Iterable,
    TypeVar,
    MutableSequence,
    MutableSequence,
    MutableSequence,
    Any,
)
from datetime import datetime

# from productMarketingDwh.models import *
# from productMarketing.models import Project
# from ..models import *
from enum import Enum
from sqlite3 import Error
import pandasql as ps
from productMarketing.models import MainCustomers, FinalCustomers  # , CustomerRelation
from productMarketingDwh.models import *
from django.db import connection


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def rawSqlPerformer(sql):
    with connection.cursor() as cursor:
        cursor.execute(sql)
        row = dictfetchall(cursor)
    return row


# fill the available error types first


class ErrorTypesVrfcOohValidation(Enum):
    # high level checks
    rfpMissing: int = 0
    # can happen when an entry in BoUp must be updated to match an updated combination in OoH. otherwise, since OoH is the source of truth for BoUp Main and End Customers, it cannot happen in the other direction.
    endCustomerMissing: int = 1
    # can happen when an entry in BoUp must be updated to match an updated combination in OoH, since OoH is the source of truth for BoUp Main and End Customers, it cannot happen in the other direction.
    mainCustomerMissing: int = 2
    mainEndCustomerMissingBoUp: int = 3
    mainEndCustomerMissingOoh: int = 4

    # low level checks, at main and end customer level (OoH row level check )
    volumeDeltaOverFive: int = 5
    volumeMissingInOoh: int = 6
    volumeMissingInBoUp: int = 7
    aspDeltaOverFive: int = 8
    priceMissingInOoh: int = 9
    priceMissingInBoUp: int = 10

    def __str__(self) -> str:
        # python < v3.10 does not support switch statements...

        if self.value == 0:
            return "RFP Missing in Bottom Up tool."
        elif self.value == 1:
            return "End customer missing completely in OoH."
        elif self.value == 2:
            return "Final customer missing completely in OoH."
        elif self.value == 3:
            return "Combination of main and final customer missing in Bottom Up tool."
        elif self.value == 4:
            return "Combination of main and final customer missing in Orders on Hand."
        elif self.value == 5:
            return "Delta of volumes between OoH and BoUp is over 5%."
        elif self.value == 6:
            return "Volume is missing in OoH."
        elif self.value == 7:
            return "Volume is missing in BoUp."
        elif self.value == 8:
            return "Delta of ASP between OoH and BoUp is over 5%."
        elif self.value == 9:
            return "Price is missing in OoH."
        elif self.value == 10:
            return "Price is missing in BoUp."


class ErrorsVrfcImport(Enum):
    # high level checks
    rfpMissing: int = 0
    quantityError: int = 1
    priceError: int = 2
    revenueError: int = 3
    objectCreationError: int = 4


def importVrfcOohCsv() -> bool:
    # datetime.datetime.now()
    now = datetime.datetime.now()
    date = now.strftime("%Y%m%d")
    totalImportErrors = []
    missingRfps = []

    """
    #####################################################################################
    Block A: data import, cleaning and normalization
    #####################################################################################

    """
    df1 = pd.read_csv(
        "./persistentLayer/importJobs/marketingCube_full_test.csv", sep=";", decimal=","
    )

    # df1 = pd.read_csv("./marketingCube_cleaned_ooh_test.csv", sep = ';', decimal=",")
    length = len(df1.index)
    index = 0
    df1 = df1.fillna(0.0)
    df1 = df1.drop([0, 1])
    df1 = df1.reset_index(drop=True)
    print("### input")
    print(df1)
    print("####")

    header = df1.columns.values.tolist()
    print("len header", len(header))
    # now remove the headers and the rows 0 and 1. the 21_22, 21_22, etc should now be the row 0.

    # 0 is the 21_22, etc.. also the fiscal year. if Total as keyword, the field is the yearly sum of FY.
    firstRow = df1.iloc[0].array
    # 1 is the label for the quarter. if 0.0, this is the yearly sum. adjust label accordingly.
    secondRow = df1.iloc[1].array
    print("second row pre import", len(secondRow))
    print(secondRow)
    print("####")

    # REV+UC = revenue and potential orders
    # REV+UC_POT_QTY = volumes
    # REV+UC_POT_ASP = asp

    for index in range(0, (len(secondRow)), 1):
        fieldName: str = ""
        headerValue: str = str(header[index])
        print("header -->", headerValue)
        # add the type of information (revenue, asp, quantity) depending on the
        if ("_QTY" in str(headerValue)) and ("PM_" in str(headerValue)):
            fieldName = "pmfc_qty_"
        elif ("ASP" in str(headerValue)) and ("PM_" in str(headerValue)):
            print("found price pm")
            fieldName = "pmfc_asp_"
        elif "PM_" in str(headerValue):
            print("found revenue pm!")
            fieldName = "pmfc_rev_"
        elif ("_QTY" in str(headerValue)) and ("S_FC" in str(headerValue)):
            print("found qty sfc")
            fieldName = "sfc_qty_"
        elif ("ASP" in str(headerValue)) and ("S_FC" in str(headerValue)):
            print("found price sfc")
            fieldName = "sfc_asp_"
        elif "S_FC_VIST" in str(headerValue):
            print("found revenue sfc!")
            fieldName = "sfc_rev_"
        elif "_QTY" in str(headerValue):
            print("found qty ooh")
            fieldName = "ooh_qty_"
        elif "ASP" in str(headerValue):
            print("found price ooh")
            fieldName = "ooh_asp_"
        elif "REV+UC_POT" in str(headerValue):
            print("found revenue ooh!")
            fieldName = "ooh_rev_"
        else:
            print("not a relevant field", headerValue)

        # if a yearly total
        if str(secondRow[index]) == str(0.0):
            secondRow[index] = fieldName + \
                str(firstRow[index].replace(" ", "_"))
        else:
            secondRow[index] = fieldName + str(secondRow[index])

    # df1.iloc[2,:] = secondRow

    df1.columns = secondRow
    df1 = df1.drop([0, 1])
    df1 = df1.reset_index(drop=True)

    # for field HFG_DESRIPTION we remove software and others 58_S, 58_9 tbd HFG_DESC
    df1 = df1[df1["HFG_DESC"] != "58_S"]
    df1 = df1[df1["HFG_DESC"] != "58_9"]

    print("df1 after import")
    print(df1)

    df1.to_csv("./test1.csv", mode="a", sep=";", decimal=",", header=True)

    """
    #####################################################################################

    Block B: data persistence customers.
    Take care of main and end customer combinations first, as well as of the respective entries in FInal and MainCustomer tables.
    #####################################################################################

    """
    # set all possible combinations to false first
    MainCustomers.objects.update(valid=False)
    FinalCustomers.objects.update(valid=False)

    # get the main customers, final customers and the respective combinations. load into FinalCustomers, MainCustomers, CustomerRelation
    uniqueMainCustomers: MutableSequence[str] = []
    uniqueFinalCustomer: MutableSequence[str] = []
    uniqueCustomerCombinations: MutableSequence[str] = []

    queryMainCustomers = """SELECT DISTINCT MC_NAME FROM df1;"""
    queryFinalCustomers = """SELECT DISTINCT EC_NAME FROM df1;"""
    queryCustomerCombinations = (
        """SELECT DISTINCT MC_NAME, EC_NAME FROM df1 ORDER BY MC_NAME ASC;"""
    )

    queryResultMainCustomers = ps.sqldf(queryMainCustomers, locals())
    queryResultFinalCustomers = ps.sqldf(queryFinalCustomers, locals())
    queryResultCustomerCombinations = ps.sqldf(
        queryCustomerCombinations, locals())

    print("main customers", type(queryResultMainCustomers))
    print(queryResultMainCustomers)
    print("end customers")
    print(queryResultFinalCustomers)
    print("end queryResultFinalCustomers2")
    print(queryResultCustomerCombinations)
    queryResultCustomerCombinations["date"] = date

    filename1 = date + "_MainEndCustomerCombinations.csv"
    path1 = "./logs/" + filename1
    queryResultCustomerCombinations.to_csv(
        path1, mode="a", sep=";", decimal=",", header=False
    )

    # unique combinations
    uniqueMainCustomers = queryResultMainCustomers["MC_NAME"].tolist()
    uniqueFinalCustomer = queryResultFinalCustomers["EC_NAME"].tolist()

    # both arrays will have the same length
    uniqueCombiMc: MutableSequence[str] = queryResultCustomerCombinations[
        "MC_NAME"
    ].tolist()

    uniqueCombiEc: MutableSequence[str] = queryResultCustomerCombinations[
        "EC_NAME"
    ].tolist()

    for index in range(0, len(uniqueMainCustomers), 1):
        obj, created = MainCustomers.objects.get_or_create(
            customerName=uniqueMainCustomers[index]
        )

        if created == True:
            print("created main customer", obj)
        else:
            print("retrieved main cust obj", obj)

        obj.valid = True
        obj.save()

    for index in range(0, len(uniqueFinalCustomer), 1):
        obj, created = FinalCustomers.objects.get_or_create(
            finalCustomerName=uniqueFinalCustomer[index]
        )

        if created == True:
            print("created final customer", obj)
        else:
            print("retrieved final cust obj", obj)

        obj.valid = True
        obj.save()

    """
    lengthA = len(queryResultCustomerCombinations.index)

    for index in range(0, lengthA, 1):
        mainCustName = queryResultCustomerCombinations.loc[index, "MC_NAME"]
        mainCustObject = MainCustomers.objects.get(customerName=mainCustName)
        finalCustStr = queryResultCustomerCombinations.loc[index, "EC_NAME"]
        fincalCustObject, created = FinalCustomers.objects.get_or_create(
            finalCustomerName=finalCustStr, mainCustomers=mainCustObject
        )

        if created == True:
            print("created final customer", fincalCustObject)
        else:
            print("retrieved final custo obj", fincalCustObject)

        obj.valid = True
        obj.save()
    """

    """
    for index in range (0, len(uniqueFinalCustomer), 1):
        obj, created = FinalCustomers.objects.get_or_create(finalCustomerName = uniqueFinalCustomer[index])
    """
    # set all previous combinations to false
    # CustomerRelation.objects.raw('UPDATE productMarketing_customerrelation SET valid = False')
    """
    CustomerRelation.objects.update(valid=False)

    ## tbd how to optimize this step, limit number of DB calls, if it's nesteable within the two previous for loops
    for index in range (0, len(uniqueCombiMc), 1):
        mcObj = MainCustomers.objects.filter(customerName = uniqueCombiMc[index])[0]
        ecObj = FinalCustomers.objects.filter(finalCustomerName = uniqueCombiEc[index])[0]
        obj, created = CustomerRelation.objects.get_or_create(mainCustomer = mcObj, finalCustomer = ecObj, valid = True)
        obj.save()

    """
    """
    #####################################################################################
    Block C: data persistence of VRFC core Data.

    Errors are related to missing RFPs in product master data, data type errors (eg floats instead of ints, etc...) or high level DB errors (IO errors)
    
    #####################################################################################
    """
    newHeader = df1.columns.values.tolist()

    # orders on hand, quantities (prices seem to be wrong)

    # truncate first??

    fiscalYears = [
        "19_20",
        "20_21",
        "21_22",
        "22_23",
        "23_24",
        "24_25",
        "25_26",
        "27_28",
        "28_29",
        "29_30",
    ]
    quarters = ["Q1", "Q2", "Q3", "Q4"]
    length = len(df1.index)

    for index in range(0, length, 1):

        errorDict = {
            "row": index,
            "errors": "",
        }

        rowValues = df1.iloc[index].array

        lengthRow = len(rowValues)
        lineLevelErrors: MutableSequence[ErrorsVrfcImport] = []

        for indexA in range(0, lengthRow, 1):
            headerValue = newHeader[indexA]

            if "ooh_qty_" in str(headerValue):

                print("found qty ooh")
                fieldName: str = "ooh_qty_"
                rfpString: str = rowValues[2]
                product: any = None
                quarter: int = 1
                quarterFound: bool = False

                # if no quarter found (total entries), this will skip
                for qr in quarters:
                    if qr in headerValue:
                        quarter = int(qr[1:])
                        quarterFound = True

                try:
                    quantity = int(rowValues[indexA])
                except:
                    # handle error
                    lineLevelErrors.append(ErrorsVrfcImport.quantityError)

                try:
                    product = Product.objects.filter(rfp=rfpString)[0]
                except:
                    # handle error
                    lineLevelErrors.append(ErrorsVrfcImport.rfpMissing)
                    missingRfps.append(rfpString)

                mainCustomerObj = MainCustomers.objects.filter(
                    customerName=rowValues[3]
                )[0]
                finalCustomerObj = FinalCustomers.objects.filter(
                    finalCustomerName=rowValues[4]
                )[0]

                if (len(lineLevelErrors) == 0) and (quarterFound == True):
                    try:
                        for fiscalYear in fiscalYears:
                            if fiscalYear in headerValue:
                                print(
                                    "creating!", product, fiscalYear, quarter, quantity
                                )
                                obj, created = VrfcOrdersOnHand.objects.get_or_create(
                                    rfp=product,
                                    mainCustomerVrfc=mainCustomerObj,
                                    endCustomerVrfc=finalCustomerObj,
                                    fiscalYear=fiscalYear,
                                    quarter=quarter,
                                    quantity=quantity,
                                )
                    except:
                        lineLevelErrors.append(
                            ErrorsVrfcImport.objectCreationError)

            elif "ooh_asp_" in str(headerValue):
                print("found price ooh")
                fieldName = "ooh_asp_"
                # will not be used
            elif "ooh_rev_" in str(headerValue):
                print("found revenue ooh!")
                fieldName = "ooh_rev_"
                # will not be used
            elif "ooh_rev_" in str(headerValue):
                print("found revenue ooh!")
                fieldName = "ooh_rev_"
                # will not be used

            # sales forecast, quantities (prices seem to be wrong)
            elif "ooh_asp_" in str(headerValue):
                print("found price ooh")
                fieldName = "ooh_asp_"
                # will not be used
            elif "ooh_rev_" in str(headerValue):
                print("found revenue ooh!")
                fieldName = "ooh_rev_"
                # will not be used
            elif "ooh_rev_" in str(headerValue):
                print("found revenue ooh!")
                fieldName = "ooh_rev_"
                # will not be used

            elif "S_FC_VIST" in str(headerValue):
                print("found revenue sfc!")
                fieldName = "sfc_rev_"
            elif "_QTY" in str(headerValue):
                print("found qty ooh")
                fieldName = "ooh_qty_"

            elif "ASP" in str(headerValue):
                print("found price ooh")

            else:
                print("field seems not relevant", headerValue)

        errorDict = {
            "row": index,
            "errors": list(dict.fromkeys(lineLevelErrors)),
        }

        totalImportErrors.append(errorDict)

    # remove duplicates
    missingRfps = list(dict.fromkeys(missingRfps))

    print("#######################################%%%%%%%%%%%%%%%%%%%%%%%")
    print("%%%%% error report %%%%%%%%")
    print(totalImportErrors)
    print("%%%%% missingRfps %%%%%")
    print(missingRfps)
    print("#######################################%%%%%%%%%%%%%%%%%%%%%%%")

    """
    #####################################################################################
    Block D: sanity checks against projects.

    
    # D1: rfp planned at main and end customer level in BoUp for a given combination of VRFC mc+ec+rfp
    # D2: func key planned in boup but missing in vrfc
    # D3: planned volume differs too much from OoH or sales forecast. tbd who to warn due to missing salesname.
    # D4: planned asp differs too much from sales forecast (ooh price is not useful). tbd who to warn due to missing salesname.

    #####################################################################################
    """
    # D1
    distinctOohMcEcRfpCombinations = VrfcOrdersOnHand.objects.raw(
        'SELECT DISTINCT rfp, mainCustomerVrfc, endCustomerVrfc  FROM productmarketingdwh_vrfcordersonhand WHERE valid = True')

    sql = "SELECT DISTINCT salesName, mainCustomer, endCustomer FROM productmarketingdwh_boup LEFT OUTER JOIN project_salesname ON (productmarketingdwh_boup.salesName = project_salesname.name)"
    distinctBoUpMcEcRfpCombinations = rawSqlPerformer(sql)

    print("unique distinctBoUpMcEcRfpCombinations",
          distinctBoUpMcEcRfpCombinations)
    return True


# importVrfcOohCsv()


"""

for reference:

    class ErrorTypesVrfcOohConflicts(models.Model):
    errorType = models.SmallIntegerField(blank=True, null=True)
    evaluationDate = models.DateTimeField(default=now, editable=False)

    class vrfcOohConflicts(models.Model):
        vrfcOohEntry = models.ForeignKey(VrfcOrdersOnHand, on_delete=PROTECT, null=True, blank=True)
        errorType = models.ForeignKey(ErrorTypesVrfcOohConflicts, on_delete=PROTECT, null=True, blank=True)
        evaluationDate = models.DateTimeField(default=now, editable=False)


for reference: 

    class VrfcOrdersOnHand(models.Model):
        rfp = models.ForeignKey(Product, on_delete=models.PROTECT, blank=True, null=False)
        mainCustomerVrfc = models.ForeignKey(MainCustomers, on_delete=models.PROTECT, blank=True, null=False)
        endCustomerVrfc = models.ForeignKey(FinalCustomers, on_delete=models.PROTECT, blank=True, null=False)
        year = models.SmallIntegerField(default=0, blank=True, null=True)
        quarter = models.SmallIntegerField(default=0, blank=True, null=True)
        quantity = models.IntegerField(default=0, blank=True, null=True)
        asp = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=4)
        revenue = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=2)

    class VrfcSalesForecast(models.Model):
        rfp = models.ForeignKey(Product, on_delete=models.PROTECT, blank=True, null=False)
        mainCustomerVrfc = models.ForeignKey(MainCustomers, on_delete=models.PROTECT, blank=True, null=False)
        endCustomerVrfc = models.ForeignKey(FinalCustomers, on_delete=models.PROTECT, blank=True, null=False)
        year = models.SmallIntegerField(default=0, blank=True, null=True)
        quarter = models.SmallIntegerField(default=0, blank=True, null=True)
        quantity = models.IntegerField(default=0, blank=True, null=True)
        asp = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=4)
        revenue = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=2)

    class VrfcPmForecast(models.Model):
        rfp = models.ForeignKey(Product, on_delete=models.PROTECT, blank=True, null=False)
        mainCustomerVrfc = models.ForeignKey(MainCustomers, on_delete=models.PROTECT, blank=True, null=False)
        endCustomerVrfc = models.ForeignKey(FinalCustomers, on_delete=models.PROTECT, blank=True, null=False)
        year = models.SmallIntegerField(default=0, blank=True, null=True)
        quarter = models.SmallIntegerField(default=0, blank=True, null=True)
        quantity = models.IntegerField(default=0, blank=True, null=True)
        asp = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=4)
        revenue = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=2)


"""
