"""
Infineon Technologies AG
The Goal is to import the entries from the Dragon.xlsx into the Dragon table while checking if the entry already exists.
Go through each row to get the End Costumer, Main Costumer.
Look up in the respective tables if exaclty one entry exists.
If more entries exists throw an error message. If no entry exist create the entry.
"""
from cmath import nan
import pandas as pd
import numpy as np
from typing import (
    Iterable,
    TypeVar,
    MutableSequence,
    MutableSequence,
    MutableSequence,
    Any,
)
from datetime import datetime
from productMarketing.models import (
    Dragon,
    FinalCustomers,
    MainCustomers,
    SalesOpportunities,
    ErrorTypesSalesOpportunities,
    SalesOpportunitiesConflicts,
    SalesName,
    Project,
    ProjectsToSalesOpportunitiesConflicts,
)

# from ..models import *
from enum import Enum


## operand: abstraction for volumes or prices
class ErrorTypesSalesOpportunitesValidation(Enum):
    # for dragon to sales opportunity
    salesNameMissing = 0
    endCustomerMissing = 1
    mainCustomerMissing = 2
    salesNameNotFound = 5  # if typo on import

    # for sales opportunity to project and viceversa
    projectNotFound = 3
    salesOpportunityNotFound = 4

    def __str__(self) -> str:
        # python < v3.10 does not support switch statements...

        if self.value == 0:
            return "salesNameMissing"
        elif self.value == 1:
            return "endCustomerMissing"
        elif self.value == 2:
            return "mainCustomerMissing"
        elif self.value == 3:
            return "projectNotFound"
        elif self.value == 4:
            return "salesOpportunityNotFound"


## to dos: type annotations
def runDragonImport():
    now = datetime.now()
    date = now.strftime("%Y%m%d")

    """
    Create the possible error types
    * sales name missing / end customer missing / final customer missing
    """

    errorTypesSalesOpportunities: MutableSequence[
        ErrorTypesSalesOpportunitesValidation
    ] = [
        ErrorTypesSalesOpportunitesValidation.salesNameMissing,
        ErrorTypesSalesOpportunitesValidation.endCustomerMissing,
        ErrorTypesSalesOpportunitesValidation.mainCustomerMissing,
    ]

    for errorType in errorTypesSalesOpportunities:
        errorObj, created = ErrorTypesSalesOpportunities.objects.get_or_create(
            errorType=errorType.value
        )
        print("created error", errorObj)

    """
    Bring the data in the wanted form.
    """

    df = pd.read_excel("./persistentLayer/importJobs/Dragon.xlsx", decimal=".")
    length = len(df.index)
    print("### input")
    print(df)
    print("####")

    """
    Go through each row to check if an entry exists already exists or if we need to create it.
    For that lookup in the currencies and product table, if the referenced object exists there.
    If we have more than one entry referenced, then we need to throw an error.
    """
    for i in range(0, length, 1):

        TRANSACTION_NUMBER_IMPORT = df.loc[i, "TRANSACTION NUMBER"]
        OPPORTUNITY_DESCRIPTION_IMPORT = df.loc[i, "1_OPPORTUNITY_DESCRIPTION"]
        CUSTOMER_CLASSIFICATION_IMPORT = df.loc[i, "CUSTOMER_CLASSIFICATION"]
        END_CUSTOMER_IMPORT = df.loc[i, "4_END_CUSTOMER"]
        DC_CHANNEL_IMPORT = df.loc[i, "DC_CHANNEL"]
        MARKET_APP_IMPORT = df.loc[i, "MARKET_APP"]
        FOCUS_PROJECT_FLAG_IMPORT = df.loc[i, "FOCUS_PROJECT_FLAG"]
        OPPORTUNITY_CHANNEL_IMPORT = df.loc[i, "OPPORTUNITY_CHANNEL"]
        DC_REGION_NAME_IMPORT = df.loc[i, "DC_REGION - NAME"]
        SOCKET_COMMENT_IMPORT = df.loc[i, "SOCKET_COMMENT"]
        ITEM_NUMBER_IMPORT = df.loc[i, "ITEM_NUMBER"]
        PL_IMPORT = df.loc[i, "2_PL"]
        HFG_IMPORT = df.loc[i, "3_HFG"]
        RFP_SP_NAME_IMPORT = df.loc[i, "RFP_SP_NAME"]
        CY_PART_FAMILY_IMPORT = df.loc[i, "CY PART FAMILY"]
        CY_PART_NAME_IMPORT = df.loc[i, "CY PART NAME"]
        MAIN_CUSTOMER_IMPORT = df.loc[i, "2_MAIN_CUSTOMER"]
        DC_CUSTOMER_IMPORT = df.loc[i, "3_DC_CUSTOMER"]
        PRODUCT_DESCRIPTION_IMPORT = df.loc[i, "3_PRODUCT_DESCRIPTION"]
        COMMENT_SALES_I_IMPORT = df.loc[i, "COMMENT_SALES_I"]
        SOCKET_COMPETITOR_IMPORT = df.loc[i, "SOCKET_COMPETITOR"]
        CREATION_DAY_IMPORT = df.loc[i, "CREATION - DAY"]
        PRODUCT_STATUS_AGGREGATED_IMPORT = df.loc[i, "PRODUCT_STATUS_AGGREGATED"]
        PRODUCT_STATUS_IMPORT = df.loc[i, "1_PRODUCT_STATUS"]
        SOCKET_STATUS_IMPORT = df.loc[i, "SOCKET_STATUS"]
        DESIGN_WIN_CLAIM_STATUS_IMPORT = df.loc[i, "DESIGN_WIN_CLAIM_STATUS_"]
        OPPORTUNITY_REASON_IMPORT = df.loc[i, "OPPORTUNITY_REASON"]
        DESIGN_LOSS_DAY_IMPORT = df.loc[i, "DESIGN_LOSS - DAY"]
        LOST_REASON_DESCRIPTION_IMPORT = df.loc[i, "LOST_REASON_DESCRIPTION"]
        SALES_FLAG_IMPORT = df.loc[i, "SALES_FLAG"]
        IFX_RESPONSIBLE_IMPORT = df.loc[i, "4_IFX_RESPONSIBLE"]
        DESIGN_WIN_EXP_MONTH_IMPORT = df.loc[i, "8_DESIGN_WIN_EXP - MONTH"]
        RAMP_UP__MONTH_IMPORT = df.loc[i, "9 RAMP_UP -  MONTH"]
        RAMP_DOWN__MONTH_IMPORT = df.loc[i, "9_RAMP_DOWN -  MONTH"]
        TRAFFIC_LIGHT_IMPORT = df.loc[i, "TRAFFIC_LIGHT"]
        TRAFFIC_LIGHT_COMMENT_IMPORT = df.loc[i, "TRAFFIC_LIGHT_COMMENT"]
        APPROVER_1_IMPORT = df.loc[i, "APPROVER 1"]
        APPROVER_2_IMPORT = df.loc[i, "APPROVER 2"]
        DW_APPR_FIN_YEAR_IMPORT = df.loc[i, "DW_APPR - FIN_YEAR"]
        DW_APPR_FIN_QUARTER_IMPORT = df.loc[i, "DW_APPR - FIN_QUARTER"]
        DW_APPR_FIN_MONTH_IMPORT = df.loc[i, "DW_APPR - FIN_MONTH"]
        DW_APPR_FIN_DAY_IMPORT = df.loc[i, "DW_APPR - FIN DAY"]
        BUSINESS_WIN_MONTH_IMPORT = df.loc[i, "BUSINESS_WIN-MONTH"]
        MAIN_CUSTOMER_NUMBER_IMPORT = df.loc[i, "MAIN_CUSTOMER - NUMBER"]
        DW_POT_UW_USD_IMPORT = df.loc[i, "DW_POT_UW_USD"]
        if np.isnan(df.loc[i, "DW_ACHIEVE_USD"]):
            DW_ACHIEVE_USD_IMPORT = 1.0
        else:
            DW_ACHIEVE_USD_IMPORT = df.loc[i, "DW_ACHIEVE_USD"]

        PLANNED_REV_UW_USD_IMPORT = df.loc[i, "PLANNED_REV_UW_USD"]
        LIFETIME_REV_USD_IMPORT = df.loc[i, "LIFETIME_REV_USD"]
        IFX_PRODUCT_QUANTITY_IMPORT = df.loc[i, "IFX_PRODUCT_QUANTITY"]
        Item_Internal_Device_IMPORT = df.loc[i, "Item: Internal Device"]
        Product_IMPORT = df.loc[i, "Product"]

        print(DW_ACHIEVE_USD_IMPORT, np.isnan(df.loc[i, "DW_ACHIEVE_USD"]))

        endCustomer = END_CUSTOMER_IMPORT  # FinalCustomers.objects.filter(finalCustomerName=END_CUSTOMER_IMPORT)
        mainCustomer = MAIN_CUSTOMER_IMPORT  #  MainCustomers.objects.filter(customerName = MAIN_CUSTOMER_IMPORT)
        # print("EndCostumer", EndCostumers, "MainCostumer", MainCostumers)

        """
        if EndCostumers.count() > 0:
            EndCostumer = EndCostumers[0]
        else:
            EndCostumer = FinalCustomers.objects.get(id = 0)
        if MainCostumers.count() > 0:
            MainCostumer = MainCostumers[0]
        else:
            MainCostumer = MainCustomers.objects.get(id = 0)
        """

        DragonObj, created = Dragon.objects.get_or_create(
            END_CUSTOMER=endCustomer,
            MAIN_CUSTOMER=endCustomer,
            RFP_SP_NAME=RFP_SP_NAME_IMPORT,  ## this is sales name
        )

        if created == True:
            print(i, "created", DragonObj)
        else:
            print(i, "retrieved", DragonObj)

        DragonObj.TRANSACTION_NUMBER = TRANSACTION_NUMBER_IMPORT
        DragonObj.OPPORTUNITY_DESCRIPTION = OPPORTUNITY_DESCRIPTION_IMPORT
        DragonObj.MARKET_APP = MARKET_APP_IMPORT
        DragonObj.PRODUCT_STATUS_AGGREGATED = PRODUCT_STATUS_AGGREGATED_IMPORT
        DragonObj.RAMP_UP_MONTH = RAMP_UP__MONTH_IMPORT
        DragonObj.RAMP_DOWN_MONTH = RAMP_DOWN__MONTH_IMPORT
        DragonObj.LIFETIME_REV_USD = LIFETIME_REV_USD_IMPORT
        DragonObj.IFX_PRODUCT_QUANTITY = IFX_PRODUCT_QUANTITY_IMPORT

        DragonObj.save()

        """
							DC_REGION_NAME = DC_REGION_NAME_IMPORT, ### region 1
							MARKET_APP = MARKET_APP_IMPORT, ### application detail
                            OPPORTUNITY_DESCRIPTION = OPPORTUNITY_DESCRIPTION_IMPORT,


                            TRANSACTION_NUMBER = TRANSACTION_NUMBER_IMPORT,
							CUSTOMER_CLASSIFICATION = CUSTOMER_CLASSIFICATION_IMPORT,
							END_CUSTOMER = endCustomer,
							DC_CHANNEL = DC_CHANNEL_IMPORT,
							FOCUS_PROJECT_FLAG = FOCUS_PROJECT_FLAG_IMPORT,
							OPPORTUNITY_CHANNEL = OPPORTUNITY_CHANNEL_IMPORT,
							SOCKET_COMMENT = SOCKET_COMMENT_IMPORT,
							ITEM_NUMBER = ITEM_NUMBER_IMPORT,
							PL = PL_IMPORT,
							HFG = HFG_IMPORT ,
							RFP_SP_NAME = RFP_SP_NAME_IMPORT ,
							CY_PART_FAMILY = CY_PART_FAMILY_IMPORT ,
							CY_PART_NAME = CY_PART_NAME_IMPORT ,
							MAIN_CUSTOMER = endCustomer ,
							DC_CUSTOMER = DC_CUSTOMER_IMPORT ,
							PRODUCT_DESCRIPTION = PRODUCT_DESCRIPTION_IMPORT ,
							COMMENT_SALES_I = COMMENT_SALES_I_IMPORT ,
							SOCKET_COMPETITOR = SOCKET_COMPETITOR_IMPORT ,
							CREATION_DAY = CREATION_DAY_IMPORT ,
							PRODUCT_STATUS = PRODUCT_STATUS_IMPORT ,
							SOCKET_STATUS = SOCKET_STATUS_IMPORT ,
							DESIGN_WIN_CLAIM_STATUS = DESIGN_WIN_CLAIM_STATUS_IMPORT ,
							OPPORTUNITY_REASON = OPPORTUNITY_REASON_IMPORT ,
							DESIGN_LOSS_DAY = DESIGN_LOSS_DAY_IMPORT ,
							LOST_REASON_DESCRIPTION = LOST_REASON_DESCRIPTION_IMPORT ,
							SALES_FLAG = SALES_FLAG_IMPORT ,
							IFX_RESPONSIBLE = IFX_RESPONSIBLE_IMPORT ,
							DESIGN_WIN_EXP_MONTH = DESIGN_WIN_EXP_MONTH_IMPORT ,

							TRAFFIC_LIGHT = TRAFFIC_LIGHT_IMPORT ,
							TRAFFIC_LIGHT_COMMENT = TRAFFIC_LIGHT_COMMENT_IMPORT ,
							APPROVER_1 = APPROVER_1_IMPORT ,
							APPROVER_2 = APPROVER_2_IMPORT ,
							DW_APPR_FIN_YEAR = DW_APPR_FIN_YEAR_IMPORT ,
							DW_APPR_FIN_QUARTER = DW_APPR_FIN_QUARTER_IMPORT ,
							DW_APPR_FIN_MONTH = DW_APPR_FIN_MONTH_IMPORT ,
							DW_APPR_FIN_DAY = DW_APPR_FIN_DAY_IMPORT ,
							BUSINESS_WIN_MONTH = BUSINESS_WIN_MONTH_IMPORT ,
							MAIN_CUSTOMER_NUMBER = MAIN_CUSTOMER_NUMBER_IMPORT ,
							DW_POT_UW_USD = DW_POT_UW_USD_IMPORT ,
							DW_ACHIEVE_USD = DW_ACHIEVE_USD_IMPORT ,
							PLANNED_REV_UW_USD = PLANNED_REV_UW_USD_IMPORT ,
							Item_Internal_Device = Item_Internal_Device_IMPORT ,
							Product = Product_IMPORT
                            )

    """

    """
    #### section A
    run sales opportunities batch import
    * functional key is product + main + end customer.
    * if key already available, update the dragon transaction id and mark as updated
    * if key not available, create a new sales opportunity
    * if integrity errors on dragon side, create entries in the conflict table regarding dragon

    #### section B
    run sales opportunities match to projects
    * if match of functional key, assign
    * if orphan from sales opportunity side, create a project draft and insert into ErrorTypesSalesOpportunities
    * if orphan from project side, create a warning / mark as not available in dragon

    """

    ################ section A
    """
    truncate the errors table first
    tbd if some kind of versioning here. evtl dump to a csv the errors table before truncating as some kind of log.
    """
    dragonObjects = Dragon.objects.all()

    ### truncate the  SalesOpportunitiesConflicts table (tbd if use a batch job identifier instead...)

    if len(dragonObjects) > 0:
        for dragonObject in dragonObjects:
            finalCustomerName = dragonObject.END_CUSTOMER
            mainCustomerName = dragonObject.MAIN_CUSTOMER
            salesName = dragonObject.RFP_SP_NAME

            mainCustomer = None
            finalCustomer = None
            salesName = None

            errorTypes: MutableSequence[ErrorTypesSalesOpportunitesValidation] = []

            try:
                finalCustomer = FinalCustomers.objects.get(
                    finalCustomerName=finalCustomerName
                )
            except:
                print("could not get an endcustomer", dragonObject)
                errorTypes.append(
                    ErrorTypesSalesOpportunitesValidation.endCustomerMissing
                )

            try:
                mainCustomer = MainCustomers.objects.get(
                    mainCustomerName=mainCustomerName
                )
            except:
                print("could not get a main customer", dragonObject)
                errorTypes.append(
                    ErrorTypesSalesOpportunitesValidation.mainCustomerMissing
                )

            try:
                salesName = SalesName.objects.get(mainCustomerName=mainCustomerName)
            except:
                print("could not get a matching sales name", dragonObject)
                errorTypes.append(
                    ErrorTypesSalesOpportunitesValidation.salesNameMissing
                )

            """"
            if on error, sales opportunity not be created, but an entry of the conflicts table.
            the import will update existing entries of sales opportunites.
            """

            if (
                (mainCustomer != None)
                and (finalCustomer != None)
                and (salesName != None)
            ):
                print("creating sales opportunity for", dragonObject)
                salesOpportunity, created = SalesOpportunities.objects.get_or_create(
                    mainCustomer=mainCustomer,
                    endCustomer=finalCustomer,
                    salesName=salesName,
                )
                salesOpportunity.dragonOpportunity = dragonObject
                salesOpportunity.save()

            else:

                print("--> ERROR: creating entry of sales opportunity conflict")
                for error in errorTypes:
                    SalesOpportunitiesConflicts.objects.get_or_create(
                        dragonOpportunity=dragonObject, errorType=error.value
                    )

    df1 = pd.DataFrame(list(SalesOpportunitiesConflicts.objects.all().values()))
    filename1 = date + "_SalesOpportunitiesConflicts.csv"
    path1 = "./logs/" + filename1
    df1.to_csv(path1, mode="a", sep=";", decimal=",", header=False)
    SalesOpportunitiesConflicts.objects.all().delete()

    """
    now, for the existing sales opportunites, check if
    * project existing
    and for the existing projects, check if
    * sales opportunity exists
    """
    ##### section B

    salesOpportunities = SalesOpportunities.objects.all()
    projects = Project.objects.all()
    ### truncate the  ProjectsToSalesOpportunitiesConflicts table (tbd if use a batch job identifier instead...)

    ProjectsToSalesOpportunitiesConflicts.objects.all().delete()

    for salesOpportunity in salesOpportunities:
        errorTypes: MutableSequence[ErrorTypesSalesOpportunitesValidation] = []

        project = Project.objects.filter(
            mainCustomer=mainCustomer, endCustomer=endCustomer, salesName=salesName
        )

        if not project:
            errorTypes.append(ErrorTypesSalesOpportunitesValidation.projectNotFound)

        ### depending on the crosschecks we implement, this could scale up to any kind of conflicts we think about
        for errorType in errorTypes:
            ProjectsToSalesOpportunitiesConflicts.objects.get_or_create(
                project=project, errorType=errorType.value
            )

    for project in projects:
        errorTypes: MutableSequence[ErrorTypesSalesOpportunitesValidation] = []

        salesOpportunity = SalesOpportunities.objects.filter(
            mainCustomer=mainCustomer, endCustomer=endCustomer, salesName=salesName
        )

        if not salesOpportunity:
            errorTypes.append(
                ErrorTypesSalesOpportunitesValidation.salesOpportunityNotFound
            )

        ### depending on the crosschecks we implement, this could scale up to any kind of conflicts we think about
        for errorType in errorTypes:
            ProjectsToSalesOpportunitiesConflicts.objects.get_or_create(
                salesOpportunity=salesOpportunity, errorType=errorType.value
            )

    df = pd.DataFrame(
        list(ProjectsToSalesOpportunitiesConflicts.objects.all().values())
    )

    filename2 = date + "_ProjectsToSalesOpportunitiesConflicts.csv"
    path2 = "./productMarketing/logs/" + filename2

    df.to_csv(path2, mode="a", sep=";", decimal=",", header=False)

    return True
