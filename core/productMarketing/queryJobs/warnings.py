from turtle import position
import numpy as np
from scipy.stats import poisson
import pandas as pd
from scipy.interpolate import interp1d
from ..models import *
from statistics import mean
from enum import Enum


# def checkConditionsAtPrice(sop, eop, prices): #check if each year has a price

#     years = list(range(sop,eop)) #False means everything ok and True means there is a missing value
#     check = False
#     for idx, i in enumerate(years):
#         if prices[idx] == 0:
#             check = True

#     return  check


def checkVolumeEntry(projectId):  #gives all years which have a volume entry
    years = []
    volumeObjects = ProjectVolumePrices.objects.filter(project_id = projectId)
    for volObj in volumeObjects:
        if volObj.quantity != 0 and not pd.isna(volObj.quantity):
            years.append(volObj.calenderYear)
            #print(volObj.quantity)
    
    return years

def checkgapsInYearsEntry(years):  #gives all years which have a volume entry
    int_years = []
    for i in years:
        int_years.append(int(i))
    check = True
    sop = min(int_years)
    eop = max(int_years)
    for i in list(range(sop,eop+1)):
        if i not in int_years:
            print("missing", i)
            check = False
    return check


'''
projectVolumePrices:
calenderYear
quantity
quantityCustomerEstimation
source
data
user #foreign key
modifiedDate
project #foreign key
valid
priceSource
comment
currency
price
priceValidityUntil
priceSourceComment
'''
### req  45,60,62
def errorDicProjectLvl(p_id):
    class Error(Enum):
        no_gaps_between_sop_eop_volume = 1
        each_vol_has_price = 2
        no_gaps_between_sop_eop_price = 3
        each_price_has_vol = 4
        price_dif_between_year_not_more_than_15_procent = 4

    projectsVolPrice = ProjectVolumePrices.objects.filter(project_id = p_id)
    errorDic = {
        Error.no_gaps_between_sop_eop_volume:True,
        Error.each_vol_has_price:True,
        Error.no_gaps_between_sop_eop_price: True,
        Error.each_price_has_vol:True,
        Error.price_dif_between_year_not_more_than_15_procent:True
               }
    
    #first check and second:gaps_between_sop_eop_volume & each_vol_has_price
    
    #list of each year with an entry
    years_vol = []
    for pVolPrice in projectsVolPrice:
        if pVolPrice.quantity != 0:
            years_vol.append(pVolPrice.calenderYear)
            if pVolPrice.price == 0:
                errorDic[Error.each_vol_has_price] = False
    #print(years_vol)

    #check if from smallest to largest year, if there is a missing year
    sop_vol = min(years_vol)
    eop_vol = max(years_vol)
    for i in list(range(sop_vol,eop_vol+1)):
        if i not in years_vol:
            errorDic[Error.no_gaps_between_sop_eop_volume] = False


    #third check and forth: gaps_between_sop_eop_price & each_price_has_vol

    #list of each year with an entry
    years_price = []
    price_list = []
    for pVolPrice in projectsVolPrice:
        if pVolPrice.price != 0:
            years_price.append(pVolPrice.calenderYear)
            price_list.append(pVolPrice.price)
            if pVolPrice.quantity == 0:
                errorDic[Error.each_price_has_vol] = False
    #print(years_price)
    #print(price_list)

    #check if from smallest to largest year, if there is a missing year
    sop_price = min(years_price)
    eop_price = max(years_price)
    for i in list(range(sop_price,eop_price+1)):
        if i not in years_price:
            errorDic[Error.no_gaps_between_sop_eop_price] = False

    #fifth check: price_dif_between_year_not_more_than_15_procent

    d = {'year': years_price, 'price': price_list}
    df_price = pd.DataFrame(data=d)

    df_sorted = df_price.sort_values(by=['year'])
    price_values = df_sorted['price'].tolist()
    #print(price_values)
    for idx, price_value in enumerate(price_values):
        if idx == 0:
            continue

        if price_value > float(price_values[idx-1])*1.15 or price_value < float(price_values[idx-1])*0.85:
            errorDic[Error.price_dif_between_year_not_more_than_15_procent] = False
    return errorDic


'''
projectVolumePrices:
calenderYear
quantity
quantityCustomerEstimation
source
data
user #foreign key
modifiedDate
project #foreign key
valid
priceSource
comment
currency
price
priceValidityUntil
priceSourceComment
'''


### req 59,61,101
def errorDicTableLvl(p_id):
    class Error(Enum):
        no_price_over_15_procent_over_silicon_average = 1
        no_price_over_30_procent_over_RFP_average = 2
        same_main_end_customer_RFP_and_same_price = 3

    errorDic = {
        Error.no_price_over_15_procent_over_silicon_average:True,
        Error.no_price_over_30_procent_over_RFP_average:True,
        Error.same_main_end_customer_RFP_and_same_price: True,
               }

    ### get a list of all projectsVolPRice with the same series (first seven characters in SalesName are the same)
   
    needed_project = Project.objects.get(id = p_id)

    projectsVolPrice_silicon =[]
    filter_string_silicon = str(needed_project.salesName)[:8]
   
    SalesNameSilicon = SalesName.objects.filter(salesName__icontains = filter_string_silicon)
    salesname_ids_silicon = SalesNameSilicon.values_list('id', flat=True)
    projectSilicon = []
    for salesname in  salesname_ids_silicon:
        for element in Project.objects.filter(salesName_id = salesname):
            #print("element:", element.id)
            projectSilicon.append(element.id)

    for identifier in projectSilicon:
        for element in ProjectVolumePrices.objects.filter(project_id = identifier):
            projectsVolPrice_silicon.append(element)

    price_list_silicon =[]
    for entry in projectsVolPrice_silicon:
        if entry.price != 0:
            price_list_silicon.append(entry.price)
    for price in price_list_silicon:
        if price > float(mean(price_list_silicon))*1.15 or price < float(mean(price_list_silicon))*0.85:
            errorDic[Error.no_price_over_15_procent_over_silicon_average] = False
    #print("test1", price_list_silicon)


    projectsVolPrice_rfp =[]
    projects_rfp =  Project.objects.filter(salesName = needed_project.salesName)
    project_ids_rfp = projects_rfp.values_list('id', flat=True)
    for identifier in project_ids_rfp:
        #print("identifier:", identifier)
        projectsVolPrice_rfp.append(ProjectVolumePrices.objects.filter(project_id = identifier))

    price_list_rfp =[]
    for query in projectsVolPrice_rfp:
        for entry in query:
            if entry.price != 0:
                price_list_rfp.append(entry.price)

    for price in price_list_rfp:
        if price > float(mean(price_list_rfp))*1.30 or price < float(mean(price_list_rfp))*0.70:
            errorDic[Error.no_price_over_30_procent_over_RFP_average] = False
    #print("test2", price_list_rfp)

    projectsVolPrice_same =[]
    projects_same =  Project.objects.filter(salesName = needed_project.salesName, mainCustomer = needed_project.mainCustomer, endCustomer = needed_project.endCustomer )
    project_ids_same = projects_same.values_list('id', flat=True)
    for identifier in project_ids_same:
        #print("same identifer", identifier)
        projectsVolPrice_same.append(ProjectVolumePrices.objects.filter(project_id = identifier))

    price_list_same =[]
    for query in projectsVolPrice_same:
        for entry in query:
            price_list_same.append(entry.price)

    for price in price_list_same:
        if price > float(mean(price_list_same))*1.01 or price < float(mean(price_list_same))*0.99:
            errorDic[Error.same_main_end_customer_RFP_and_same_price] = False
    
    return errorDic

def checkBoUpEntryGapsVol(df):  
    dfFilteredVol = df.loc[df['vol'] != 0]
    groups = dfFilteredVol['id'].unique()
    check_list=[]
    for group in groups:
        dfVolGroup = dfFilteredVol.loc[dfFilteredVol['id'] == group]
        check = True
        sop = min(dfVolGroup.year)
        eop = max(dfVolGroup.year)
        for i in list(range(sop,eop+1)):
            if i not in  set(dfVolGroup.year):
                check = False
        check_list.append((group,check))
    print(check_list)
    return check_list

def checkBoUpEntryGapsPrice(df): 
    dfFilteredPrice = df.loc[df['price'] != 0]
    groups = dfFilteredPrice['id'].unique()
    check_list=[]
    for group in groups:
        dfPriceGroup = dfFilteredPrice.loc[dfFilteredPrice['id'] == group]
        check = True
        sop = min(dfPriceGroup.year)
        eop = max(dfPriceGroup.year)
        for i in list(range(sop,eop+1)):
            if i not in  set(dfPriceGroup.year):
                #print("problem row", i )
                check = False
        check_list.append((group,check))
        #print("years of group just done", dfPriceGroup.year, "project_id", group)
    #print(check_list)
    return check_list


