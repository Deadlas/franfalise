from turtle import position
import numpy as np
from scipy.stats import poisson
import pandas as pd
from scipy.interpolate import interp1d


def arraySum(arr):
    sum = 0
    for i in arr:
        sum = sum + i

    return(sum)


def interpolator(sop, eop, initialVolume, peakVolume, peakYear, distributionType, totalVolume):
    print("interpolating ----->!")

    # estimate total volume

    # poison
    if True:

        x = []

        if totalVolume != 0:
            # lam = peak year, relative
            relPeak = peakYear - sop
            print("rel peak", relPeak, "total volume", totalVolume)

            # lambda determines how far away is the production peak from the eop. the farther away, the larger the lambda value should be.
            # to do: iterate with lambdas until peak is reached at peak year...
            # to do: iterate until peak volume is respected
            lambdaValue = relPeak

            # do distribution for 10 years
            for i in range(0, eop-sop, 1):
                x.append(poisson.pmf(i, lambdaValue))
            print("count", len(x), "sum", arraySum(x))

            for i in range(0, eop-sop, 1):
                x[i] = x[i] * totalVolume

            print("---> final poisson distribution", x)

            return x


def checkConditions(sop, eop, initialVolume, peakVolume, peakYear, distributionType, totalVolume):

    eopSmallerEopError = False
    sopLargerPeakError = False
    peakLargerEopError = False
    peakVolError = False
    initialVolError = False
    totalVolumeError = False

    # only peak, sop, total volume
    if distributionType == "poisson":

        if int(sop) > int(peakYear):
            print("unplausible dates! 2")
            sopLargerPeakError = True

        if int(totalVolume) < 500:
            totalVolumeError = True
            print("unplausible total volume")

            """
            if sop >= eop:
                print("unplausible dates! 1")
                eopSmallerEopError = True

            if sop > peakYear:
                print("unplausible dates! 2")
                sopLargerPeakError = True

            if peakYear > eop:
                peakLargerEopError = True
                print("unplausible dates! 3")

            if peakVolume == 0:
                peakVolError = True
                print("unplausible volumes! 1")

            if initialVolume == 0:
                initialVolError = True
                print("unplausible volumes! 2")

            if (totalVolume < initialVolume) | (totalVolume < peakVolume):
                totalVolumeError = True
                print("unplausible total volume")
            """

    return eopSmallerEopError, sopLargerPeakError, peakLargerEopError, peakVolError, initialVolError, totalVolumeError


def yearToMonthPoissonSmoother(sop, eop, totalVolume, peakYear):

    print("poisson distribution for year to month ----->!")

    # estimate total volume

    # poison
    if True:
        x = []
        if totalVolume != 0:
            # lam = peak year, relative
            relPeakMonth = (peakYear - sop) * 12
            print("rel peak", relPeakMonth, "total volume", totalVolume)

            # lambda determines how far away is the production peak from the eop. the farther away, the larger the lambda value should be.
            # to do: iterate with lambdas until peak is reached at peak year...
            # to do: iterate until peak volume is respected
            lambdaValue = relPeakMonth
            monthInTotal = 12*(eop-sop)
            # do distribution for 10 years
            for i in range(0, monthInTotal, 1):
                x.append(poisson.pmf(i, lambdaValue))
            print("count", len(x), "sum", arraySum(x))

            for i in range(0, monthInTotal, 1):
                x[i] = x[i] * totalVolume

            print("---> final poisson distribution", x)

            return x


'''
Input: StartOfProdcution, EndOfProduction (the start and end year), totalVolume= an array with the total volume for each year
Output: an array which has the volume for each month 
'''


def calc_m_t(sop, eop, totalVolume):
    sop = int(sop)
    eop = int(eop)
    years = list(range(sop, eop+1))
    print("using", sop, eop, "years", years, "total volume", totalVolume)
    years_adj = [x-sop for x in years]
    totalVolume_adj = [int(x)/12 for x in totalVolume]
    print("totalVolume_adj", totalVolume_adj)
    print("years_adj", years_adj)

    waypointsx = []
    waypointsV = []
    waypointsV.append(totalVolume_adj[0])
    waypointsV.append(totalVolume_adj[1])
    waypointsx.append(0)
    waypointsx.append(1)
    m = []
    t = []
    for i in years_adj:  # [:-1]
        # first y=mx + t
        if i == 0:
            t.append(totalVolume_adj[0])
            m.append(totalVolume_adj[1] - t[0])
            waypointsV.append(m[0]*1.5+t[0])
            waypointsx.append(1.5)
        elif i == 1:
            continue
        else:
            print("totalVolume_adj", totalVolume_adj, "trying to use i", i)

            waypointsV.append(totalVolume_adj[i])
            waypointsx.append(i)

            m_val = (waypointsV[-1]-waypointsV[-2]) / \
                (waypointsx[-1]-waypointsx[-2])

            t.append(waypointsV[-1]-m_val * waypointsx[-1])
            m.append(m_val)

            waypointsV.append(m[i-1]*(i+0.5)+t[i-1])
            waypointsx.append(i+0.5)
    boundries = waypointsx[::2]

    return m, t, boundries


def linSmoother(sop, eop, totalVolume):
    m, t, boundries = calc_m_t(sop, eop, totalVolume)
    resultvol = []
    print(boundries)
    for i in range(1, len(boundries)*12+1):

        x_val = i/12 - 0.5

        if x_val < boundries[0]:
            new_val = x_val*m[0]+t[0]
            if new_val < 0:
                resultvol.append(0)
            else:
                resultvol.append(new_val)

        if x_val >= boundries[-1]:
            new_val = x_val*m[-1]+t[-1]
            if new_val < 0:
                resultvol.append(0)
            else:
                resultvol.append(new_val)

        for idx, _ in enumerate(boundries):
            if idx == len(boundries)-1:
                continue
            if x_val < boundries[idx+1] and x_val >= boundries[idx]:
                new_val = x_val*m[idx]+t[idx]
                if new_val < 0:
                    resultvol.append(0)
                else:
                    resultvol.append(x_val*m[idx]+t[idx])
    # print("first year", totalVolume[0])   #small error because its only an approximation
    # print(sum(resultvol[:12]))
    #print("second year", totalVolume[1])
    # print(sum(resultvol[12:24]))
    return resultvol
