# Francisco Falise, copyright 01/10/2022

from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from .views import *

urlpatterns = [
    path('productMarketing/boupEntry', views.boupEntry, name='boupEntry'),

    path('productMarketing/boupEntry1/<int:projectId>', views.volumeEntry, name='volumeEntry'),
    path('productMarketing/boupEntry2/<int:projectId>', views.priceSelection, name='priceSelection'),
    path('productMarketing/boupEntry2a/<int:projectId>', views.priceEnterExcel, name='priceEnterExcel'),

    path('productMarketing/boupEntry3/<int:projectId>/<int:mode>', views.priceConfirmation, name='priceConfirmation'),
    path('productMarketing/boupEntry4/<int:projectId>', views.boupEntryOverview, name='boupEntryOverview'),
    path('productMarketing/boupSaveAndSubmit/<int:projectId>', views.saveAndSubmit, name='boupEntryOverview'),
    path('productMarketing/boupSaveAsDraft/<int:projectId>', views.saveAsDraft, name='saveAsDraft'),
### import jobs
    path('productMarketing/salesNamesImportJob', salesNamesImportJob.as_view()),
    path('productMarketing/vhklImportJob', vhklImportJob.as_view()),
    path('productMarketing/DragonImportJob', DragonImportJob.as_view()),
    path('productMarketing/imports', views.fullImports, name='imports'),

#### import views
    path('productMarketing/importExport', DragonImportJob.as_view(), name='import_export'),

### edit a project
    path('productMarketing/projectEdit/<int:case>/<int:projectId>', views.projectEdit, name='projectEdit'),
    path('productMarketing/projectManagement/<int:case>', views.projectManagement, name='projectManagement'),



]

htmxurlpatterns = [
    path('productMarketing/dropdownApplicationDetail', views.dropdownApplicationDetail, name='dropdownApplicationDetail'),
    path('productMarketing/dropdownProductSeries', views.dropdownProductSeries, name='dropdownProductSeries'),
    path('productMarketing/dropdownPackage/<str:family>', views.dropdownPackage, name='dropdownProductSalesName'),
    path('productMarketing/dropdownProductSalesName/<str:family>/<str:series>', views.dropdownProductSalesName, name='dropdownProductSalesName'),
    path('productMarketing/distributionConfigurator/<int:projectId>', views.distributionConfigurator, name='distributionConfigurator'),
]

urlpatterns += htmxurlpatterns
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)