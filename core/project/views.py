from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import View
from django.urls import reverse_lazy, reverse
from .models import Project
from .forms import ProjectCreateForm, VolumeForm, PricingForm, VolumeAutomaticForm, VolumeMonthForm
from productMarketing.excelFormInputValidator import checkExcelFormInput, TypeOfParameter
from productMarketingDwh.models import *
from enum import Enum
from productMarketing.interpolator import *
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.db.models import F
from .models import Project, Product, SalesName, ProductPackage, ProductSeries
from django.utils import timezone
from datetime import datetime


class CreateProjectView(LoginRequiredMixin, View):
    template_name = "project/project_create_view/step1.html"
    form_class = ProjectCreateForm
    success_url = reverse_lazy("create_volume_view")
    editMode = False

    def get(self, request, *args, **kwargs):
        context = {"form": self.form_class()}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request=request)
        # if submitted form is valid
        series = form["product_series"]
        print("series", request.POST.get("product_series"))
        print("post, salesname", request.POST.get("sales_name"))
        salesNameInput = request.POST.get("sales_name")

        salesNameObj = None

        # if it's an int, then it was selected without filtering by series, package, etc...
        try:
            salesNameInt = int(salesNameInput)
            salesNameObj = SalesName.objects.get(id=salesNameInt)

        except:
            salesNameObj = SalesName.objects.get(name=salesNameInput)

        # remember old state
        _mutable = request.POST._mutable
        # set to mutable
        request.POST._mutable = True
        request.POST["sales_name"] = salesNameObj.pk
        print("salesnameobj", salesNameObj, "pk", salesNameObj.pk)
        print("salesname output request", request.POST["sales_name"])

        # set mutable flag back
        request.POST._mutable = _mutable

        if form.is_valid():
            # project saved in db
            project = form.save()
            project.user = request.user
            project.save()
            print("################ project form is valid. assigned ID", project.id)
            # stored project id in session to get in future
            request.session["project_id"] = project.id
            return redirect(self.success_url)
        else:
            for error in form.errors:
                print("### error:", error)
            print("form is not valid")
        context = {"form": self.form_class(request.POST, request=request)}
        return render(request, self.template_name, context)


def EditProjectDraftEntry(request, projectId):
    print("########### selecting volume entry")
    redirect_url = reverse_lazy("edit_project_draft_view")
    request.session["project_id"] = projectId
    return redirect(redirect_url)


class EditProjectDraftView(LoginRequiredMixin, View):
    template_name = "project/project_create_view/step1.html"
    form_class = ProjectCreateForm
    success_url = reverse_lazy("create_volume_view")

    def get(self, request, *args, **kwargs):
        print("edit view, kwargs", kwargs)
        # = projectId  # 11kwargs["projectId"]
        projectId = request.session["project_id"]
        project = Project.objects.get(id=projectId)
        print("### editing project ->", project, "projectId", project.id)
        context = {"form": self.form_class(
            instance=project, editMode=True), "editMode": True}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        projectId = request.session["project_id"]
        project = Project.objects.get(id=projectId)
        form = self.form_class(request.POST, request=request,
                               editMode=True, instance=project)
        series = form["product_series"]
        salesNameInput = request.POST.get("sales_name")
        salesNameObj = None

        try:
            salesNameInt = int(salesNameInput)
            salesNameObj = SalesName.objects.get(id=salesNameInt)

        except:
            salesNameObj = SalesName.objects.get(name=salesNameInput)

        _mutable = request.POST._mutable
        request.POST._mutable = True
        request.POST["sales_name"] = salesNameObj.pk
        request.POST._mutable = _mutable

        if form.is_valid():
            # project saved in db
            project = form.save()
            project.user = request.user
            project.save()
            print("################ project form is valid. assigned ID",
                  project.id, "app main", project.applicationMain)
            # stored project id in session to get in future
            request.session["project_id"] = project.id
            return redirect(self.success_url)
        else:
            for error in form.errors:
                print("### error:", error)
            print("form is not valid")
        context = {"form": self.form_class(request.POST, request=request)}
        return render(request, self.template_name, context)


def SelectVolumeEntryType(request):
    print("########### selecting volume entry")

    if request.method == 'POST':
        print("post")

    return render(request, "project/project_create_view/step2a.html", {'segment': 'boupOverviewSlim', })


class CreateVolumeExcelView(LoginRequiredMixin, View):
    print("$$$$$ running CreateVolumeExcelView")
    template_name = "project/project_create_view/step2b_excel.html"
    form_class = VolumeForm
    success_url = reverse_lazy("create_pricing_view")

    def get(self, request, *args, **kwargs):
        # getting project instance that was created in first step
        project_id = request.session.get("project_id", None)
        if project_id:
            project = Project.objects.get(id=project_id)
            # getting estimated SOP from created project
            estimatedSop = project.estimatedSop
            # feeding that sop to startOfProduction field
            initial_dict = {"startOfProduction": estimatedSop}

            # check if volumes are already existing for this project. if yes, prefill the jexcel with it.
            volumes = []
            years = []
            dataCondition = 0

            try:
                objects = ProjectVolumePrices.objects.filter(
                    project=project)

                if objects.count() > 0:
                    dataCondition = 1
                    for object in objects:
                        volumes.append(object.quantity)
                        years.append(object.calenderYear)
            except:
                pass
            # to avoid problem with empty volumes

            if len(years) == 0:
                dataCondition = 0
            print("lean yeears", len(years),
                  "resulting data condition", dataCondition)

            context = {"form": self.form_class(
                initial=initial_dict), "dataCondition": dataCondition, "volumes": volumes, "years": years}
            print("rendering view with sop", estimatedSop,
                  "volumes", volumes, "years", years, "data condition", dataCondition)
            return render(request, self.template_name, context)
        else:
            return redirect(reverse("create_project_view"))

    def post(self, request, *args, **kwargs):
        project_id = request.session.get("project_id", None)
        project = Project.objects.get(id=project_id)
        estimatedSop = project.estimatedSop
        initial_dict = {"startOfProduction": estimatedSop}

        form = self.form_class(request.POST, initial=initial_dict)
        print("initialized form after post with project ID", project_id)
        if form.is_valid():
            # process the data submitted in the form...
            # following data will be extracted after running clean method defined under VolumeForm in forms.py file
            # field_value = form.cleaned_data.get("field_name")
            print("form is valid!")
            project = Project.objects.get(id=project_id)
            volumes = form.volume  # form.cleaned_data.get("volume")
            years = form.years  # form.cleaned_data.get("years")
            print("form cleaned data volumes", volumes)
            print(len(years))
            volumesPost = []

            # first disable all the existing volume entries for this project.
            currentVolumes = ProjectVolumePrices.objects.filter(
                project=project)
            currentMonthVolumes = ProjectVolumeMonth.objects.filter(
                project=project)
            currentVolumes.update(valid=False)
            currentMonthVolumes.update(valid=False)

            # now redo the entries
            for i in range(0, (len(years)), 1):
                object, created = ProjectVolumePrices.objects.get_or_create(
                    project=project, calenderYear=int(years[i]))
                object.quantity = int(volumes[i])
                object.valid = True
                object.user = request.user

                if created == False:
                    object.modifiedDate = datetime.now(tz=timezone.utc)

                object.save()
                volumesPost.append(int(volumes[i]))
                print(created, "---> volume object,", object)

            year = 0
            monthVolume = linSmoother(years[0], years[-1], volumes)
            print("months:", len(monthVolume),
                  "years", years, "last", years[-1])
            for i in range(0, (len(years))*12, 1):

                if i != 0 and i % 12 == 0:
                    #print(int(years[year]), "----", int(volumes[i]))
                    year = year + 1
                month = (i % 12)+1  # +1 so we dont have the 0th month
                volumeObjectM, created = ProjectVolumeMonth.objects.get_or_create(
                    project=project, calenderYear=int(years[year]), month=month)
                volumeObjectM.quantity = monthVolume[i]
                volumeObjectM.valid = True
                volumeObjectM.user = request.user

                if created == False:
                    object.modifiedDate = datetime.now(tz=timezone.utc)

                volumeObjectM.save()
                # volumesPost.append(f(int(years[0])+0.5+(i/12)))
                print(i)
                print(created, "---> volume month object,", volumeObjectM)

            # $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ TO DO: Patric: implement transaction reverse on fail!, see atomic, non atomic transactions
            """
            # prepare the volume confirmation form
            firstYear = years[0]
            lastYear = years[len(years) - 1]
            deltaYears = int(firstYear) - 2020
            print("pre volumes array", volumesPost)
            print("last year", lastYear)
            if deltaYears > 0:
                for i in range(0, deltaYears, 1):
                    volumesPost.insert(0, 0)

            deltaYearsEnd = 2045 - int(lastYear)
            print("deltaYearsEnd", deltaYearsEnd, deltaYearsEnd > 0)

            if (deltaYearsEnd > 0):
                for i in range(0, deltaYearsEnd, 1):
                    volumesPost.append(0)
                    print("##")
            """
            return redirect(self.success_url)

        else:

            excelData = request.POST.get("excelData")
            years = []
            volumes = []
            dataCondition = 0
            print("excelData", excelData)

            try:
                yearsA, volumesA = excelData.split("\n")
                print("excelData", excelData)
                years = yearsA.strip().split(",")
                volumes = volumesA.strip().split(",")
                volumes = [float(x) for x in volumes]
                years = [float(x) for x in years]
                print("years", years, "volumes", volumes)
                dataCondition = 1
            except:
                print("failed completely to recover entered data... sorry")

            if dataCondition == 1:
                if len(years) == 0:
                    dataCondition = 0
            print("lean yeears", len(years),
                  "resulting data condition", dataCondition)

            context = {"form": self.form_class(request.POST,
                                               initial=initial_dict), "dataCondition": dataCondition, "volumes": volumes, "years": years}

            #context = {"form": form}
            return render(request, self.template_name, context)


class CreateVolumeExcelMonthView(LoginRequiredMixin, View):
    print("$$$$$ running CreateVolumeExcelView")
    template_name = "project/project_create_view/step2c_excelMonth.html"
    form_class = VolumeMonthForm
    success_url = reverse_lazy("create_pricing_view")

    # get does not change wrt to CreateVolumeExcelView
    def get(self, request, *args, **kwargs):
        project_id = request.session.get("project_id", None)
        if project_id:
            project = Project.objects.get(id=project_id)
            estimatedSop = project.estimatedSop
            initial_dict = {"startOfProduction": estimatedSop}
            context = {"form": self.form_class(initial=initial_dict)}
            print("rendering view with sop", estimatedSop)
            return render(request, self.template_name, context)
        else:
            return redirect(reverse("create_project_view"))

    def post(self, request, *args, **kwargs):
        project_id = request.session.get("project_id", None)
        project = Project.objects.get(id=project_id)
        estimatedSop = project.estimatedSop
        initial_dict = {"startOfProduction": estimatedSop}

        form = self.form_class(request.POST, initial=initial_dict)
        print("initialized form after post with project ID", project_id)

        if form.is_valid():
            print("form is valid!")
            project = Project.objects.get(id=project_id)
            volumes = form.cleaned_data.get("volume")
            years = form.cleaned_data.get("years")
            months = form.cleaned_data.get("months")
            print(len(years))

            # tbd...
            """
            volumesPost = []
            for i in range(0, (len(years)), 1):
                object, created = ProjectVolumePrices.objects.get_or_create(
                    project=project, calenderYear=int(years[i]))
                object.quantity = int(volumes[i])
                object.save()
                volumesPost.append(int(volumes[i]))
                print(created, "---> volume object,", object)

            year = 0
            monthVolume = linSmoother(years[0], years[-1], volumes)
            print("months:", len(monthVolume),
                  "years", years, "last", years[-1])
            for i in range(0, (len(years))*12, 1):

                if i != 0 and i % 12 == 0:
                    #print(int(years[year]), "----", int(volumes[i]))
                    year = year + 1
                month = (i % 12)+1  # +1 so we dont have the 0th month
                volumeObjectM, created = ProjectVolumeMonth.objects.get_or_create(
                    project=project, calenderYear=int(years[year]), month=month)
                volumeObjectM.quantity = monthVolume[i]
                volumeObjectM.save()
                # volumesPost.append(f(int(years[0])+0.5+(i/12)))
                print(i)
                print(created, "---> volume month object,", volumeObjectM)

            # prepare the volume confirmation form
            firstYear = years[0]
            lastYear = years[len(years) - 1]
            deltaYears = int(firstYear) - 2020
            print("pre volumes array", volumesPost)
            print("last year", lastYear)
            if deltaYears > 0:
                for i in range(0, deltaYears, 1):
                    volumesPost.insert(0, 0)

            deltaYearsEnd = 2045 - int(lastYear)
            print("deltaYearsEnd", deltaYearsEnd, deltaYearsEnd > 0)

            if (deltaYearsEnd > 0):
                for i in range(0, deltaYearsEnd, 1):
                    volumesPost.append(0)
                    print("##")

            return redirect(self.success_url)
            """
        else:

            context = {"form": form}
            return render(request, self.template_name, context)


class CreateVolumeAutomaticView(LoginRequiredMixin, View):
    template_name = "project/project_create_view/step2b_automatic.html"
    form_class = VolumeAutomaticForm
    success_url = reverse_lazy("create_pricing_view")

    def get(self, request, *args, **kwargs):
        # getting project instance that was created in first step
        project_id = request.session.get("project_id", None)
        if project_id:
            project = Project.objects.get(id=project_id)
            # getting estimated SOP from created project
            estimatedSop = project.estimatedSop
            # feeding that sop to startOfProduction field
            initial_dict = {"startOfProduction": estimatedSop}
            context = {"form": self.form_class(initial=initial_dict)}
            return render(request, self.template_name, context)
        else:
            return redirect(reverse("create_project_view"))

    def post(self, request, *args, **kwargs):
        project_id = request.session.get("project_id", None)
        project = Project.objects.get(id=project_id)
        estimatedSop = project.estimatedSop

        # feeding that sop to startOfProduction field
        initial_dict = {"startOfProduction": estimatedSop}
        context = {"form": self.form_class(initial=initial_dict)}
        form = self.form_class(request.POST, initial=initial_dict)

        if form.is_valid():
            # process the data submitted in the form...
            # following data will be extracted after running clean method defined under VolumeForm in forms.py file
            # field_value = form.cleaned_data.get("field_name")
            print("prices form is valid!")
            excelData = form.cleaned_data.get("excelData")

            # here automatic volume distribution
            print("starting interpolation routine!!!")
            excelData = form.cleaned_data.get("excelData")

            sop = int(estimatedSop)
            # request.POST.get("endOfProduction") # volumeAutomaticForm.endOfProduction
            eop = int(form.cleaned_data.get("endOfProduction"))
            # initialVolume = request.POST.get("initialVolume") # volumeAutomaticForm.initialVolume
            # peakVolume = request.POST.get("peakVolume")  #volumeAutomaticForm.peakVolume
            # request.POST.get("peakYear")  #volumeAutomaticForm.peakYear
            peakYear = int(form.cleaned_data.get("peakYear"))
            # distributionType = request.POST.get("distributionType") # volumeAutomaticForm.distributionType
            # request.POST.get("totalVolume")
            totalVolume = int(form.cleaned_data.get("totalVolume"))
            print("sop, eop, initialVolume, peakVolume, peakYear, distributionType, totalVolume--------->",
                  sop, eop, peakYear, totalVolume)

            """
            if sop != None:
                sop = int(sop)
                if int(sop) != sop:
                    print("sop changed!!")
                    project.estimatedSop = int(sop)
                    project.save()
            """

            #########
            print("starting interpolation!!!")
            # for distribution on year level
            monthLevel = False
            if monthLevel == False:
                interpolationResults = interpolator(
                    int(sop), int(eop), None, None, int(peakYear), None, int(totalVolume))
                print("interpolation results!!!", interpolationResults)

                # the interpolation result is an array... it's 0 element is of SoP year
                interpolationResultsMonth = yearToMonthPoissonSmoother(
                    int(sop), int(eop), int(totalVolume), int(peakYear))
                print("interpolation results month!!!",
                      interpolationResultsMonth)

                # if the interpolation succeeded, first disable all the existing volume entries for this project.
                currentVolumes = ProjectVolumePrices.objects.filter(
                    project=project)
                currentMonthVolumes = ProjectVolumeMonth.objects.filter(
                    project=project)
                currentVolumes.update(valid=False)
                currentMonthVolumes.update(valid=False)

                ##########
                # insert into volumes table
                years = []
                year = sop
                for index in range(0, 10, 1):
                    quantity = int(interpolationResults[index])
                    volumeObject, created = ProjectVolumePrices.objects.get_or_create(
                        project=project, calenderYear=year)
                    print("volume object", volumeObject, "quantity", quantity)
                    volumeObject.user = request.user
                    years.append(year)
                    year = year + 1
                    volumeObject.quantity = quantity

                    volumeObject.valid = True
                    volumeObject.user = request.user

                    if created == False:
                        volumeObject.modifiedDate = datetime.now(
                            tz=timezone.utc)
                    volumeObject.save()

                """
                %%%%%%%%%%%%%%%%%%%%%%%%%%%% frage an patrict -> warum ist die logik für year vs. monthly hier getrennt???
                """
                #########

                # the interpolation is an array of arrays... it's 0 element is of SoP year, its 0 0 element is first month of SoP
                ##########
                # insert into volumes month table
                year = sop
                month = 1
                for index in range(0, (eop-sop)*12, 1):
                    if month == 13:
                        month = 1
                        year = year + 1

                    quantity = int(interpolationResultsMonth[index])
                    volumeObject, created = ProjectVolumeMonth.objects.get_or_create(
                        project=project, calenderYear=year, month=month)
                    print("volume object", volumeObject,
                          "created", created, "quantity", quantity)

                    volumeObject.user = request.user
                    volumeObject.quantity = quantity
                    volumeObject.month = month
                    volumeObject.valid = True

                    if created == False:
                        volumeObject.modifiedDate = datetime.now(
                            tz=timezone.utc)

                    volumeObject.save()
                    month = month+1

                """
                %%%%%%%%%%%%%%%%%%%%%%%%%%%% frage an patrict -> warum ist die logik hier getrennt???
                """
            else:

                interpolationResults = yearToMonthPoissonSmoother(
                    sop, eop, totalVolume, peakYear)
                print("interpolation results!!!", interpolationResults)

                # the interpolation is an array of arrays... it's 0 element is of SoP year, its 0 0 element is first month of SoP
                ##########
                # insert into volumes month table
                year = sop
                month = 1
                for index in range(0, (eop-sop)*12, 1):
                    if month == 13:
                        month = 1
                        year = year + 1

                    quantity = int(interpolationResults[index])
                    volumeObject, created = ProjectVolumeMonth.objects.get_or_create(
                        project=project, calenderYear=year, month=month)
                    print("volume object", volumeObject,
                          "created", created, "quantity", quantity)

                    volumeObject.user = request.user
                    volumeObject.quantity = quantity
                    volumeObject.month = month
                    volumeObject.save()
                    month = month+1

            # show volume confirmation screen!

            print("redirecting...")
            return redirect(self.success_url)

        context = {"form": self.form_class(request.POST)}
        return render(request, self.template_name, context)


class CreatePricingView(LoginRequiredMixin, View):
    template_name = "project/project_create_view/step3.html"
    form_class = PricingForm
    success_url = reverse_lazy("create_volume_view")

    def get(self, request, *args, **kwargs):
        project_id = request.session.get("project_id", None)
        project = Project.objects.get(id=project_id)
        # feeding that sop to startOfProduction field
        initial_dict = {"startOfProduction": project.estimatedSop}

        # check if prices are already existing for this project. if yes, prefill the jexcel with it.
        prices = []
        years = []
        dataCondition = 0

        try:
            objects = ProjectVolumePrices.objects.filter(
                project=project)

            if objects.count() > 0:
                dataCondition = 1
                for object in objects:
                    prices.append(object.price)
                    years.append(object.calenderYear)
        except:
            pass

        # to avoid problem with empty prices
        if len(years) == 0:
            dataCondition = 0
        print("lean yeears", len(years),
              "resulting data condition", dataCondition)

        context = {"form": self.form_class(
            initial=initial_dict), "prices": prices, "years": years, "dataCondition": dataCondition}
        #context = {"form": self.form_class(initial=initial_dict)}
        #context = {"form": self.form_class()}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        project_id = request.session.get("project_id", None)
        project = Project.objects.get(id=project_id)
        initial_dict = {"startOfProduction": project.estimatedSop}
        form = self.form_class(request.POST, initial=initial_dict)

        if form.is_valid():
            # process the data submitted in the form...
            # following data will be extracted after running clean method defined under VolumeForm in forms.py file
            # field_value = form.cleaned_data.get("field_name")

            # to do: check that for all valid entries of volumes there is also a price entered... otherwise raise exception.

            # clear project id from session
            try:
                del request.session["project_id"]
            except KeyError:
                pass
            context = {}
            return render(request, "project/project_create_view/done.html", context)
        context = {"form": self.form_class(request.POST)}
        return render(request, self.template_name, context)


@csrf_exempt
def series_list_json(request):
    if request.method == "POST":
        query = request.POST.get("query")
        if query:
            product_series = (
                ProductSeries.objects.filter(family__id=query)
                .annotate(text=F("series"))
                .values("id", "text")
            )
            return JsonResponse({"data": list(product_series)}, safe=True)
        return JsonResponse({}, safe=True)


@csrf_exempt
def package_list_json(request):
    if request.method == "POST":
        query = request.POST.get("query")
        if query:
            product_packages = (
                ProductPackage.objects.filter(series__id=query)
                .annotate(text=F("package"))
                .values("id", "text")
            )
            return JsonResponse({"data": list(product_packages)}, safe=True)
        return JsonResponse({}, safe=True)


@csrf_exempt
def sales_name_json(request):
    if request.method == "POST":
        query = request.POST.get("query")
        if query:
            product_ids = Product.objects.filter(package__package=query).values_list(
                "id", flat=True
            )
            print("product ids, plural", product_ids)
            sales_names = (
                SalesName.objects.filter(rfp__id__in=product_ids)
                .annotate(text=F("name"))
                .values("id", "text")
            )
            return JsonResponse({"data": list(sales_names)}, safe=True)
        return JsonResponse({}, safe=True)
