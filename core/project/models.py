from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from smart_selects.db_fields import ChainedForeignKey
from django.db.models import UniqueConstraint
# Create your models here.

# getting used user model
User = get_user_model()


class ApplicationLine(models.Model):
    applicationLineShortName = models.CharField(max_length=10)
    applicationLineLongName = models.CharField(max_length=20)


class LegalEntities(models.Model):
    leShortName = models.CharField(max_length=5)
    leLongName = models.CharField(max_length=35)


class marketerMetadata(models.Model):

    name = models.CharField(max_length=40, blank=True, null=True)
    familyName = models.CharField(max_length=40, blank=True, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=1)
    country = models.CharField(max_length=15, blank=True, null=True)
    legalEntity = models.ForeignKey(
        LegalEntities, on_delete=models.PROTECT, blank=True, null=True
    )
    applicationLine = models.ForeignKey(
        ApplicationLine, on_delete=models.PROTECT, blank=True, null=True
    )  # default = "ATV.PSE"

    def __str__(self):
        return f"{self.name} {self.familyName}"


class ApplicationMain(models.Model):
    appMainDescription = models.CharField(max_length=40, blank=True, null=True)

    def __str__(self):
        return self.appMainDescription


class ApplicationDetail(models.Model):

    appDetailDescription = models.CharField(
        max_length=40, blank=True, null=True)
    appMain = models.ForeignKey(
        ApplicationMain, on_delete=models.PROTECT, blank=True, null=True
    )

    def __str__(self):
        return self.appDetailDescription


class MainCustomers(models.Model):
    customerName = models.CharField(max_length=40, blank=True, null=True)
    valid = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return self.customerName


class FinalCustomers(models.Model):
    # relation removed upon agreement with customer on nov 25th
    """
    mainCust = models.ForeignKey(
        MainCustomers, on_delete=models.SET_NULL, null=True
    )
    """
    finalCustomerName = models.CharField(max_length=40, blank=True, null=True)
    valid = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return self.finalCustomerName
    """
    class Meta:
        unique_together = ["mainCust", "finalCustomerName"]
    """


class ProductFamily(models.Model):
    family_name = models.CharField(max_length=200)
    valid = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return self.family_name


class ProductSeries(models.Model):
    family = models.ForeignKey(ProductFamily, on_delete=models.CASCADE)
    series = models.CharField(max_length=200)
    description = models.CharField(max_length=40, blank=True, null=True)
    valid = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return self.series


class ProductPackage(models.Model):
    series = models.ForeignKey(ProductSeries, on_delete=models.CASCADE)
    package = models.CharField(max_length=200)
    description = models.CharField(max_length=40, blank=True, null=True)
    valid = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return self.package


class Product(models.Model):
    hfg = models.CharField(max_length=20, blank=True, null=True)
    ppos = models.CharField(max_length=30, blank=True, null=True)
    rfp = models.CharField(max_length=40, blank=True, null=True)
    package = models.ForeignKey(
        ProductPackage, on_delete=models.SET_NULL, null=True)
    basicType = models.CharField(max_length=40, blank=True, null=True)
    availablePGS = models.CharField(max_length=40, blank=True, null=True)
    valid = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return f"{self.rfp}"


class SalesName(models.Model):
    rfp = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=40)
    dummy = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return self.name


class Distributors(models.Model):
    distributorName = models.CharField(max_length=40, blank=True, null=True)

    def __str__(self):
        return self.distributorName


class EMS(models.Model):
    emsName = models.CharField(max_length=40, blank=True, null=True)

    def __str__(self):
        return self.emsName


class Tier1(models.Model):
    tierOneName = models.CharField(max_length=40, blank=True, null=True)

    def __str__(self):
        return self.tierOneName


class OEM(models.Model):
    oemName = models.CharField(max_length=40, blank=True, null=True)

    def __str__(self):
        return self.oemName


class SalesContacts(models.Model):

    name = models.CharField(max_length=40, blank=True, null=True)
    deptarment = models.CharField(max_length=40, blank=True, null=True)

    def __str__(self):
        return self.name


class Project(models.Model):

    ALLOWABLE_TYPES_PROJECT_STATUS = (
        ("100", "BW (100%)"),
        ("90", "DW (90%)"),
        ("60", "DI (60%)"),
        ("40", "DI (40%)"),
        ("10", "OP (10%)"),
        ("0", "LO (0%)"),
    )

    ALLOWABLE_TYPES_REGION = (
        ("EMEA", "EMEA"),
        ("AMERICAS", "AMERICAS"),
        ("AP", "AP"),
        ("JAPAN", "JAPAN"),
        ("GC", "GC"),
        ("DIVERSE", "DIVERSE"),
    )

    ALLOWABLE_TYPES_DCCHANNEL = (
        ("direct", "Direct"),
        ("distribution", "Distribution"),
    )

    ALLOWABLE_TYPES_PRICE_STATUS = (
        ("estim", "Estimation"),
        ("quote", "Quotation"),
        ("contr", "Contract"),
    )

    productMarketer = models.ForeignKey(
        marketerMetadata, on_delete=models.PROTECT, verbose_name="Product Marketer"
    )
    spNumber = models.IntegerField(blank=True, null=True)

    applicationMain = models.ForeignKey(
        ApplicationMain, on_delete=models.PROTECT, verbose_name="Application Main"
    )

    applicationDetail = ChainedForeignKey(
        ApplicationDetail,
        chained_field="applicationMain",
        chained_model_field="appMain",
        show_all=False,
        auto_choose=True,
        sort=True,
        on_delete=models.CASCADE,
        null=True,
        verbose_name="Application Detail",
    )

    familyPriceApplicable = models.BooleanField(
        default=False, blank=True, null=True)
    familyPriceDetails = models.CharField(max_length=50, blank=True, null=True)
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, blank=True, null=True)
    modifiedDate = models.DateTimeField(auto_now=True)
    creationDate = models.DateTimeField(auto_now_add=True)

    estimatedSop = models.IntegerField(
        blank=True,
        null=True,
        default=2020,
        validators=[MinValueValidator(2020), MaxValueValidator(2050)],
        verbose_name="Estimated SOP",
    )
    status = models.CharField(
        max_length=15, choices=ALLOWABLE_TYPES_PROJECT_STATUS, default=0
    )
    region = models.CharField(
        max_length=10, choices=ALLOWABLE_TYPES_REGION, default="EMEA"
    )
    secondRegion = models.CharField(
        max_length=10,
        choices=ALLOWABLE_TYPES_REGION,
        blank=True,
        null=True,
        verbose_name="Second Region",
    )

    dcChannel = models.CharField(
        max_length=12, choices=ALLOWABLE_TYPES_DCCHANNEL, blank=True, null=True
    )
    valid = models.BooleanField(default=True, blank=True, null=True)
    mainCustomer = models.ForeignKey(
        MainCustomers,
        on_delete=models.PROTECT,
        blank=False,
        null=True,
        verbose_name="Main Customer",
    )

    finalCustomer = models.ForeignKey(
        FinalCustomers,
        on_delete=models.PROTECT,
        blank=False,
        null=True,
        verbose_name="Final Customer",
    )

    # chained foreign key removed after customer alignment
    """
    finalCustomer = ChainedForeignKey(
        FinalCustomers,
        chained_field="mainCustomer",
        chained_model_field="mainCust",
        verbose_name="Final Customer",
        show_all=False,
        auto_choose=True,
        sort=True,
        null=True,
    )
    """
    # salesName = models.ForeignKey(
    #     SalesName, on_delete=models.PROTECT, blank=True, null=True
    # )

    """
    product_family = models.ForeignKey(
        ProductFamily, on_delete=models.SET_NULL, null=True
    )
    product_series = ChainedForeignKey(
        ProductSeries,
        chained_field="product_family",
        chained_model_field="family",
        show_all=False,
        auto_choose=True,
        sort=True,
        null=True,
    )
    product_package = ChainedForeignKey(
        ProductPackage,
        chained_field="product_series",
        chained_model_field="series",
        show_all=False,
        auto_choose=True,
        sort=True,
        null=True,
    )

    product = ChainedForeignKey(
        Product,
        chained_field="product_package",
        chained_model_field="package",
        show_all=False,
        auto_choose=False,
        sort=True,
        null=True,
    )
    """
    sales_name = models.ForeignKey(
        SalesName,
        on_delete=models.SET_NULL,
        null=True,
    )

    comment = models.TextField(blank=True, null=True)
    projectName = models.CharField(max_length=150, verbose_name="Project Name")
    projectDescription = models.CharField(
        max_length=400, default="", blank=True, null=True
    )
    draft = models.BooleanField(default=True, blank=True, null=True)
    priceValidUntil = models.DateField(blank=True, null=True)
    priceType = models.CharField(
        max_length=5, choices=ALLOWABLE_TYPES_PRICE_STATUS, blank=True, null=True
    )

    # comment for Saadat: while entering data, if a distributor / ems / tier1 or OEM is not available on the dropdown list, the user should be able to create a new one on the fly
    distributor = models.ForeignKey(
        Distributors, on_delete=models.PROTECT, blank=True, null=True
    )
    ems = models.ForeignKey(
        EMS, on_delete=models.PROTECT, blank=True, null=True, verbose_name="EMS"
    )
    tier1 = models.ForeignKey(
        Tier1, on_delete=models.PROTECT, blank=True, null=True, verbose_name="Tier 1"
    )
    oem = models.ForeignKey(
        OEM, on_delete=models.PROTECT, blank=True, null=True, verbose_name="OEM"
    )

    vpaCustomer = models.BooleanField(default=False, blank=True, null=True)

    salesContact = models.ForeignKey(
        SalesContacts,
        on_delete=models.PROTECT,
        blank=True,
        null=True,
        verbose_name="Sales Contact",
    )

    projectReviewed = models.BooleanField(default=False, blank=True, null=True)
    reviewDate = models.DateField(blank=True, null=True)

    def __str__(self):
        return f"{self.mainCustomer}_{self.finalCustomer}_{self.sales_name}"

    class Meta:
        constraints = [
            UniqueConstraint(
                fields=['mainCustomer', 'finalCustomer', 'draft',
                        'user', 'applicationDetail', 'applicationMain', 'sales_name'],
                name='unique_together_project',
            ),
        ]
