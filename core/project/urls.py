from django.urls import path, re_path
from . import views

urlpatterns = [
    path(
        "create/",
        views.CreateProjectView.as_view(),
        name="create_project_view",
    ),

    path(
        "editDraft/",
        views.EditProjectDraftView.as_view(),
        name="edit_project_draft_view",
    ),

    path('editDraft/<int:projectId>', views.EditProjectDraftEntry,
         name='edit_project_draft_entry'),

    path('volumeTypeSelect/',
         views.SelectVolumeEntryType, name='create_volume_view'),
    path(
        "volumeAutomatic/",
        views.CreateVolumeAutomaticView.as_view(),
        name="create_volume_automatic_view",
    ),
    path(
        "volumeExcel/",
        views.CreateVolumeExcelView.as_view(),
        name="create_volume_excel_view",
    ),
    path(
        "volumeExcelMonth/",
        views.CreateVolumeExcelMonthView.as_view(),
        name="create_volume_excel_month_view",
    ),
    path(
        "pricing/",
        views.CreatePricingView.as_view(),
        name="create_pricing_view",
    ),
    path("series-lists/", views.series_list_json, name="series_list_json"),
    path("package-lists/", views.package_list_json, name="package_list_json"),
    path("sales-names-lists/", views.sales_name_json,
         name="sales_name_list_json"),

]
