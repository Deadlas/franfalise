from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from productMarketing.excelFormInputValidator import checkExcelFormInput, TypeOfParameter
from django.utils.safestring import mark_safe
# from betterforms.multiform import MultiForm, MultiModelForm
from django.urls import reverse
from .models import Project, ProductFamily, ProductPackage, ProductSeries, MainCustomers, FinalCustomers
from enum import Enum
from typing import Any, Iterable, MutableSequence, TypeVar
import datetime

product_families = list(ProductFamily.objects.values_list("id", "family_name"))
product_families.insert(0, ("", "---------"))


# operand: abstraction for volumes or prices
class errorTypesAutomaticDistribution(Enum):
    other = 0
    eopSmallerEopError = 1
    sopLargerPeakError = 2
    peakLargerEopError = 3
    peakVolError = 4
    initialVolError = 5
    totalVolumeError = 6

    def __str__(self):
        # python < v3.10 does not support switch statements...

        if self.value == 0:
            return "Miscelaneous error found."
        elif self.value == 1:
            return "End of Production is before Start of Production. "
        elif self.value == 2:
            return "Start of Production is after Peak Year. "
        elif self.value == 3:
            return "Peak Year is after End of Production. "
        elif self.value == 4:
            return "Peak volume error. "
        elif self.value == 5:
            return "Initial volume error. "
        elif self.value == 6:
            return "Lifetime volume error. It's either 0 or unplausible."


def checkConditionsAutomaticVolume(sop: int, eop: int, peakYear: int, totalVolume: int) -> MutableSequence[errorTypesAutomaticDistribution]:
    errorMutableSequence: MutableSequence[errorTypesAutomaticDistribution] = []

    # only peak, sop, total volume
    if int(sop) > int(peakYear):
        print("unplausible dates! 2")
        #sopLargerPeakError = True
        errorMutableSequence.append(
            errorTypesAutomaticDistribution.sopLargerPeakError)

    if int(totalVolume) < 500:
        totalVolumeError = True
        print("unplausible total volume")
        errorMutableSequence.append(
            errorTypesAutomaticDistribution.totalVolumeError)

    if sop >= eop:
        print("unplausible dates! 1")
        eopSmallerEopError = True
        errorMutableSequence.append(
            errorTypesAutomaticDistribution.eopSmallerEopError)

    if sop > peakYear:
        print("unplausible dates! 2")
        sopLargerPeakError = True
        errorMutableSequence.append(
            errorTypesAutomaticDistribution.sopLargerPeakError)

    if peakYear > eop:
        peakLargerEopError = True
        print("unplausible dates! 3")
        errorMutableSequence.append(
            errorTypesAutomaticDistribution.peakLargerEopError)
    """
    if peakVolume == 0:
        peakVolError = True
        print("unplausible volumes! 1")
        errorMutableSequence.append(
            errorTypesAutomaticDistribution.peakLargerEopError)

    """

    """


    if initialVolume == 0:
        initialVolError = True
        print("unplausible volumes! 2")

    if (totalVolume < initialVolume) | (totalVolume < peakVolume):
        totalVolumeError = True
        print("unplausible total volume")

    """

    return errorMutableSequence


class ProjectCreateForm(forms.ModelForm):
    estimatedSop = forms.IntegerField(
        max_value=2050, min_value=2020, initial=2020)
    # sales filrters
    product_family = forms.CharField(
        widget=forms.Select(choices=product_families), required=False
    )
    product_package = forms.CharField(widget=forms.Select(), required=False)
    product_series = forms.CharField(widget=forms.Select(), required=False)
    #sales_name = forms.CharField(widget=forms.Select(), required=False)
    editMode = False

    class Meta:
        model = Project
        fields = (
            # first tab fields
            "projectName",
            "productMarketer",
            "region",
            "secondRegion",
            "estimatedSop",
            "status",
            "salesContact",
            "comment",
            # 2nd tab fields
            "mainCustomer",
            "finalCustomer",
            "distributor",
            "ems",
            "tier1",
            "oem",
            "vpaCustomer",
            # 3rd tab fields
            # "product_family",
            "product_series",
            "product_package",
            "sales_name",
            "applicationMain",
            "applicationDetail",
        )

        labels = {
            'sales_name': 'Sales Name',
        }

    def __init__(self, *args, **kwargs):
        print("initializing project form, kwargs", kwargs)

        # in order to override unique contraint check for edit mode. pop before calling super in order to avoid crash.
        try:
            self.editMode = kwargs.pop('editMode', False)  # kwargs["editMode"]
            print("edit mode found when initializing project create form", self.editMode)
        except:
            print("no edit mode found in project create form", self.editMode)

        try:
            self.request = kwargs.pop('request', None)
            print("--> popped request", kwargs.pop('request', None))
        except:
            pass

        super(ProjectCreateForm, self).__init__(*args, **kwargs)

        self.fields["mainCustomer"].queryset = self.fields["mainCustomer"].queryset.order_by(
            "customerName")
        self.fields["finalCustomer"].queryset = self.fields["finalCustomer"].queryset.order_by(
            "finalCustomerName")

    def clean(self):
        cleaned_data = super().clean()
        mainApplicationObj = cleaned_data.get("applicationMain")
        applicationDetailObj = cleaned_data.get("applicationDetail")
        salesNameObj = cleaned_data.get("sales_name")
        mainCustomerObj = cleaned_data.get("mainCustomer")
        finalCustomerObj = cleaned_data.get("finalCustomer")
        user = self.request.user
        print("AA using user", user)

        print("%%% data inputs", mainApplicationObj, applicationDetailObj,
              salesNameObj, mainCustomerObj, finalCustomerObj, "self edit mode", self.editMode)

        # check if unique constraint
        # if editing a project, skip this.
        if (self.editMode == False):
            print("testing if project exists")
            try:
                query = Project.objects.get(applicationMain__appMainDescription=mainApplicationObj, applicationDetail__appDetailDescription=applicationDetailObj,
                                            sales_name__name=salesNameObj, mainCustomer__customerName=mainCustomerObj, finalCustomer__finalCustomerName=finalCustomerObj, user=user, draft=True)

                redirectUrl = reverse('edit_project_draft_entry', kwargs={
                    'projectId': query.id})
                print("---> project exists!", query,
                      "redirect URL", redirectUrl)

                raise ValidationError(mark_safe(
                    ('You already created a project draft. Please edit it under <a href="{0}">this link</a>.').format(redirectUrl)))

            except Project.DoesNotExist:
                print("project did not exist!!")

        sop = cleaned_data.get("estimatedSop")
        today = datetime.date.today()
        year = today.year

        if (sop - year) > 2:
            raise ValidationError(
                'Start of Production is too far away in the past! Please review your data.')


class VolumeForm(forms.Form):
    # for excel copy paste value
    excelData = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={"class": "d-none"},
        ),
    )
    # automatic distribution fields
    startOfProduction = forms.DecimalField(label="SoP Year", required=False)
    volume = []
    years = []

    def __init__(self, *args, **kwargs):

        super(VolumeForm, self).__init__(*args, **kwargs)
        self.fields["excelData"].label = ""
        self.fields["startOfProduction"].widget.attrs["readonly"] = True
        self.fields["startOfProduction"].widget.attrs["hidden"] = True
        self.volumes = []
        self.years = []

        try:
            self.fields["startOfProduction"].initial = kwargs["initial"]["startOfProduction"]
        except:
            print("did not find initial values!")

    def clean(self):
        cleaned_data = super().clean()
        excelData = cleaned_data.get("excelData")
        startOfProduction = cleaned_data.get("startOfProduction")
        print("cleaning form!!! with sop", startOfProduction)
        if excelData:

            # the front end already filters and keeps the two first rows, unless we are entering data on a monthly basis.
            # the form was filled with copy, paste excel field
            excelDataRowCount: int = len(excelData.split("\n"))
            print("$$$ count of excel data rows", excelDataRowCount)

            if excelDataRowCount != 2:
                raise ValidationError(
                    "You need to enter two rows if working on calendar year basis!"
                )

            # try:
            years, volume = excelData.split("\n")
            print("##### volume form split, years", years)
            print("##### volume form split, volumes", volume)

            yearsCheck, outputDataCheck, errorMutableSequence = checkExcelFormInput(
                excelData=excelData, dataType=TypeOfParameter.volumes, sop=startOfProduction)
            print("stepB", errorMutableSequence, len(errorMutableSequence))

            if len(errorMutableSequence) > 0:
                errorString = ""
                print("1234")
                errorMutableSequence = list(
                    dict.fromkeys(errorMutableSequence))
                for error in errorMutableSequence:
                    errorString = errorString + error.__str__()

                print("$$$$$$$$$$$$$$$$$$$$$$ final error string:", errorString)
                print("12345")

                raise ValidationError(
                    str(errorString)
                )

            else:
                print("statement A")
                self.years = yearsCheck
                self.volume = outputDataCheck
                print("statement b", self.years, "volumes", self.volume)

                cleaned_data["excelData"] = excelData

        else:
            print("##### raising validation error!")
            # case nothing was entered return on error
            raise ValidationError(mark_safe(('Input required! If you do not want to enter data manually, please go to the <a href="{0}">Automatic Volume Distribution</a>.'
                                             ).format(reverse('create_volume_automatic_view'))))

        return cleaned_data


class VolumeMonthForm(VolumeForm):

    def clean(self):
        cleaned_data = super().clean()
        excelData = cleaned_data.get("excelData")
        startOfProduction = cleaned_data.get("startOfProduction")
        print("cleaning form!!! with sop", startOfProduction)
        if excelData:

            # the front end already filters and keeps the two first rows, unless we are entering data on a monthly basis.
            # the form was filled with copy, paste excel field
            excelDataRowCount: int = len(excelData.split("\n"))
            print("$$$ count of excel data rows", excelDataRowCount)

            if excelDataRowCount != 3:
                raise ValidationError(
                    "You need to enter three rows if working on a monthly input basis!"
                )

            # try:
            years, months, volume = excelData.split("\n")
            print("##### volume form split, years", years)
            print("##### volume form split, volumes", volume)

            yearsCheck, outputDataCheck, errorMutableSequence = checkExcelFormInput(
                excelData=excelData, dataType=TypeOfParameter.volumes, sop=startOfProduction)
            print("stepB", errorMutableSequence, len(errorMutableSequence))

            if len(errorMutableSequence) > 0:
                errorString = ""
                errorMutableSequence = list(
                    dict.fromkeys(errorMutableSequence))
                for error in errorMutableSequence:
                    errorString = errorString + error.__str__()

                raise ValidationError(
                    str(errorString)
                )

            else:
                cleaned_data["years"] = years
                cleaned_data["months"] = months
                cleaned_data["volume"] = volume
                cleaned_data["excelData"] = excelData

        else:
            print("##### raising validation error!")
            # case nothing was entered return on error
            raise ValidationError(mark_safe(('Input required! If you do not want to enter data manually, please go to the <a href="{0}">Automatic Volume Distribution</a>.'
                                             ).format(reverse('create_volume_automatic_view'))))

        return cleaned_data


class VolumeAutomaticForm(forms.Form):

    # automatic distribution fields
    startOfProduction = forms.DecimalField(label="SoP Year", required=False)
    endOfProduction = forms.DecimalField(
        label="EoL Year", initial=2030, required=False)
    totalVolume = forms.DecimalField(
        label="Total Volume", initial=0, required=False)
    peakYear = forms.DecimalField(
        label="Peak Year", initial=2025, required=False)

    def __init__(self, *args, **kwargs):
        super(VolumeAutomaticForm, self).__init__(*args, **kwargs)
        self.fields["startOfProduction"].widget.attrs["readonly"] = True
        self.fields["startOfProduction"].widget.attrs["hidden"] = False

        try:
            self.fields["startOfProduction"].initial = kwargs["initial"]["startOfProduction"]
        except:
            print("did not find initial values!")

    def clean(self):
        cleaned_data = super().clean()
        startOfProduction = cleaned_data.get("startOfProduction")
        print("cleaning form!!!")

        # either form was filled with automatic volume distribution or nothing was entered
        eop = cleaned_data["endOfProduction"]
        totalVolume = cleaned_data["totalVolume"]
        peakYear = cleaned_data["peakYear"]
        sop = cleaned_data["startOfProduction"]

        NoneType = type(None)
        if (type(eop) != NoneType) & (type(totalVolume) != NoneType) & (type(peakYear) != NoneType) & (type(sop) != NoneType):
            print("entered eop, totalVolume, peakYear, sop",
                  eop, peakYear, sop)

            # validations...
            # plausi checks:
            # sop < eop,
            # sop <= peak year <= eop,
            # peak volume != 0
            # initial volume > 0

            errorMutableSequence = checkConditionsAutomaticVolume(
                sop, eop, peakYear, totalVolume)

            if len(errorMutableSequence) > 0:
                errorString = ""
                errorMutableSequence = list(
                    dict.fromkeys(errorMutableSequence))
                for error in errorMutableSequence:
                    errorString = errorString + error.__str__()

                raise ValidationError(
                    str(errorString)
                )

            else:
                return cleaned_data

        else:
            print("%%%%%%%%%%%%% raising validation error, inputs are required!!!")
            # case nothing was entered return on error
            raise ValidationError(
                "Inputs are required!"
            )

        return cleaned_data


class PricingForm(forms.Form):
    currency = forms.ChoiceField(choices=(("USD", "USD"), ("EUR", "EURO")))
    price_commitment_until = forms.IntegerField(min_value=2020, max_value=2050)
    startOfProduction = forms.DecimalField(label="SoP Year", required=False)
    comment = forms.CharField()
    excelData = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={"class": "d-none"},
        ),
    )

    def __init__(self, *args, **kwargs):
        super(PricingForm, self).__init__(*args, **kwargs)
        self.fields["excelData"].label = ""
        self.fields["startOfProduction"].widget.attrs["readonly"] = True
        self.fields["startOfProduction"].widget.attrs["hidden"] = True

        try:
            self.fields["startOfProduction"].initial = kwargs["initial"]["startOfProduction"]
        except:
            print("did not find initial values!")

    def clean(self):
        cleaned_data = super().clean()
        excelData = cleaned_data.get("excelData")
        startOfProduction = cleaned_data.get("startOfProduction")

        if excelData:

            # the front end already filters and keeps the two first rows, unless we are entering data on a monthly basis.
            # the form was filled with copy, paste excel field
            excelDataRowCount: int = len(excelData.split("\n"))

            if excelDataRowCount != 2:
                raise ValidationError(
                    "You need to enter two rows if working on calendar year basis!"
                )

            # try:
            years, prices = excelData.split("\n")

            yearsCheck, outputDataCheck, errorMutableSequence = checkExcelFormInput(
                excelData=excelData, dataType=TypeOfParameter.prices, sop=startOfProduction)
            print("stepB", errorMutableSequence, len(errorMutableSequence))

            if len(errorMutableSequence) > 0:
                errorString = ""
                print("1234")
                errorMutableSequence = list(
                    dict.fromkeys(errorMutableSequence))
                for error in errorMutableSequence:
                    errorString = errorString + error.__str__()

                raise ValidationError(
                    str(errorString)
                )

            else:
                print("statement b")
                cleaned_data["years"] = years
                cleaned_data["prices"] = prices
                cleaned_data["excelData"] = excelData

        else:
            print("##### raising validation error!")
            # case nothing was entered return on error
            raise ValidationError("Inputs required!")

        if excelData:
            # the form was filled with copy, paste excel field
            try:
                years, prices = excelData.split("\n")

                yearsCheck, outputDataCheck, errorMutableSequence = checkExcelFormInput(
                    excelData=excelData, dataType=TypeOfParameter.prices, sop=startOfProduction)

                if len(errorMutableSequence) > 0:
                    errorString = ""

                    for error in errorMutableSequence:
                        errorString.append(error.__str__())

                    print("$$$$$$$$$$$$$$$$$$$$$$ final error string:", errorString)
                    raise ValidationError(
                        errorString
                    )

                else:

                    cleaned_data["years"] = yearsCheck
                    cleaned_data["prices"] = outputDataCheck
                    cleaned_data["excelData"] = excelData

            except:
                raise ValidationError(
                    "Enter Year in first row and Volume in second row"
                )

        return cleaned_data
